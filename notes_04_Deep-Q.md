# Deep Reinforcement Learning (Deep Q-Learning)
## Deep Q-Learning
We alerdy have all the tools to make Deep Q-Learning work. It is a DNN beig used as a function approximator for Q(s, a).  
Instead of x = feature_expansion(s, a) we will use x = feature_exansion(s)  
Each output node will correstond to a different action.

### Experience Replay
Use a replay buffer, a list that stores 4-tuples (s,a,r,s'). Do not do SGD wrt most recent TD error. Instead sample from a random mini-batch from the buffer, do GD wrt the batch. Buffer is a queue (FIFO).  
When training, we want a good representation of the true data distribution. With the default Q update, we always learn from the beginning to the end of an episode. It introduces hidden patterns and unwanted correlations. It is like training the NN by giving all images of cats and the all images of dogs (no random sorting).  
Training happens on every step. If there is not enough experience in the buffer, build the buffer with random experience first.

### Semi-Gradients
In Q-Learning, the Q approx itself is ued as the target for TD error so the gradient is not a true gradient but a semi-gradient. This can introduce instability in DQNs.

### Dual Networks
The solution is to introduce another DQN, the "target network". It is responsible for creting the target for the TD error. Is a copy of the "main" DQN but not updated as often. Helps stabilize the targets.

```python
while not done:
    if i % 100 == 0:
        targetNetwork.copy_from(mainNetwork)
```

### Dealing with images
Use CNNs. It is not possible to get all necessay information from 1 frame so we use multiple frames in our state representation.  

state(t) = [image(t-3), image(t-2), image(t-1), image(t)]  
Stack 4 frames: (4, H, W, C)  
Convert to grayscale: (4, H, W)  
Now use as a 3D input to a CNN.

### Summary
- DQN: approximate Q(s, a) with |A| output nodes
- Experience replay: train from a buffer of (s,a,r,s') touples
- Target network: for generating TD target
- CNN with past 4 frames as the state
