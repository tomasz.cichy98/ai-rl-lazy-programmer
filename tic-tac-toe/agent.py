import numpy as np
import matplotlib.pyplot as plt

LENGTH = 3

class Agent:
    def __init__(self, eps = 0.1, alpha = 0.5):
        self.eps = eps
        self.epsList = []
        self.alpha = alpha
        self.verbose = False
        self.state_history = []

    def set_V(self, V):
        self.V = V

    def set_symbol(self, sym):
        self.sym = sym

    def set_verbose(self, verbose):
        self.verbose = verbose

    def reset_history(self):
        self.state_history = []

    def take_action(self, env, t, is_gui = False):
        # eps-greedy
        pos2value = {}
        r = np.random.rand()
        best_state = None

        # decaying epsilon
        self.epsList.append( np.random.rand() / (t/10+1) )
        self.eps = self.epsList[-1]

        if r < self.eps:
            # random action
            if self.verbose:
                print("Taking random action")

            possible_moves = []
            for i in range(LENGTH):
                for j in range(LENGTH):
                    if env.is_empty(i, j):
                        possible_moves.append((i,j))
            idx = np.random.choice(len(possible_moves))
            next_move = possible_moves[idx]

        else:
            # choose best move
            if self.verbose:
                print("Taking greedy action")

            next_move = None
            best_value = -1
            for i in range(LENGTH):
                for j in range(LENGTH):
                    if env.is_empty(i,j):
                        # what would be the state if we took this move
                        env.board[i,j] = self.sym
                        state = env.get_state()
                        env.board[i,j] = 0
                        # convert states to values
                        pos2value[(i, j)] = self.V[state]
                        # init any next move
                        next_move = (i,j)
                        # find best value                    
                        if self.V[state] > best_value:
                            best_value = self.V[state]
                            best_state = state
                            next_move = (i, j)
        
            # draw board
            if self.verbose:
                for i in range(LENGTH):
                    print("------------------")
                    for j in range(LENGTH):
                        if env.is_empty(i,j):
                            # print values in empty fields
                            print("%.2f |" % pos2value[(i, j)], end="")
                        else:
                            # print symbols in full fields
                            print("  ", end="")
                            if env.board[i,j] == env.x:
                                print("x  |", end = "")
                            elif env.board[i,j] == env.o:
                                print("o  |", end = "")
                            else:
                                print("  |", end = "")
                    print("")
                print("-----------------")
        env.board[next_move[0], next_move[1]] = self.sym

    def update_state_history(self, s):
        self.state_history.append(s)
    
    def update(self, env):
        """
        Backtrack over states so
        V(prev_state) = V(prev_state) + alpha*(V(next_state) - V(prev_state))

        NOTE: ONLY do this at the end of an episode
        """
        reward = env.reward(self.sym)
        target = reward

        for prev in reversed(self.state_history):
            value = self.V[prev] + self.alpha*(target - self.V[prev])
            self.V[prev] = value
            target = value
        self.reset_history()

    def plot_epsilon(self):
        plt.plot(self.epsList, label = r'$\frac{\mathit{rand}}{\frac{\mathit{t}}{10} + 1}$')
        plt.legend(prop = {"size": 20})
        plt.title("Epsilon Greedy esplion decaying over time")
        plt.xlabel("log(time)")
        plt.ylabel("epsilon")
        plt.xscale("log")
        plt.show()

