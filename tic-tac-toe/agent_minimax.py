import numpy as np
import matplotlib.pyplot as plt
import time

LENGTH = 3

class Agent_Minimax:
    def __init__(self, eps = 0.1, alpha = 0.5):
        self.eps = eps
        self.epsList = []
        self.alpha = alpha
        self.verbose = False
        self.state_history = []

    def set_V(self, V):
        self.V = V

    def set_symbol(self, sym):
        self.sym = sym

    def set_verbose(self, verbose):
        self.verbose = verbose

    def reset_history(self):
        self.state_history = []

    def take_action(self, env, t, is_gui = False):
        print("Minimaxing...")
        start = time.time()

        move_value = {}
        # find all possible moves
        possible_moves = []
        for i in range(LENGTH):
            for j in range(LENGTH):
                if env.is_empty(i, j):
                    possible_moves.append((i,j))

        for move in possible_moves:
            env.board[move[0], move[1]] = -1                # make a move then minimax from it
            move_value[move] = self.minimax(env, 0, float("-inf"), float("inf"), is_max_player = True)
            env.board[move[0], move[1]] = 0                 # revert the move

        next_move = min(move_value, key=move_value.get)     # find move with minimal value (computer is a minimizing player)
        env.board[next_move[0], next_move[1]] = self.sym    # make a move

        end = time.time()
        print(f"Time elapsed:{round(end - start, 3)}")
      
    def minimax(self, env, depth, alpha, beta, is_max_player = True):
        if env.game_over():
            # deal with Null winner (draw)
            value = env.winner
            if env.winner == None:
                value = 0
            # reset
            env.winner = None
            env.ended = False
            return value

        possible_moves = []
        for i in range(LENGTH):
            for j in range(LENGTH):
                if env.is_empty(i, j):
                    possible_moves.append((i,j))
            
        if is_max_player:
            max_eval = float("-inf")
            for move in possible_moves:
                env.board[move[0], move[1]] = 1
                curr_eval = self.minimax(env, depth, alpha, beta, is_max_player = False)
                env.board[move[0], move[1]] = 0
                max_eval = max(max_eval, curr_eval)
                # alpha beta pruning
                alpha = max(alpha, curr_eval)
                if beta >= alpha:
                    break
            return max_eval

        if not is_max_player:
            min_eval = float("inf")
            for move in possible_moves:
                env.board[move[0], move[1]] = -1
                curr_eval = self.minimax(env, depth, alpha, beta, is_max_player = True)
                env.board[move[0], move[1]] = 0
                min_eval = min(min_eval, curr_eval)
                # alpha beta pruning
                beta = min(beta, curr_eval)
                if alpha >= beta:
                    break
            return min_eval

    #  not needed for this agent
    def update_state_history(self, s):
        pass
    
    def update(self, env):
        pass


