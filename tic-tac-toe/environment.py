import numpy as np

LENGTH = 3

class Environment:
    def __init__(self):
        self.board = np.zeros((LENGTH, LENGTH))
        self.x = -1 # represent x on the board, player 1
        self.o = 1  # player 2
        self.winner = None
        self.ended = False
        self.num_states = 3**(LENGTH*LENGTH)

    def is_empty(self, i,j):
        return self.board[i, j] == 0
    
    def reward(self, sym):
        if not self.game_over():
            return 0
        return 1 if self.winner == sym else -1

    def get_state(self):
        """
        Return current state in form of an int
        It's a base 3 number
        """
        k = 0
        h = 0 # hash

        for i in range(LENGTH):
            for j in range(LENGTH):
                # encode board
                if self.board[i, j] == 0:
                    v = 0
                elif self.board[i, j] == self.x:
                    v = 1
                elif self.board[i, j] == self.o:
                    v = 2
                h += (3**k) * v
                k += 1
        return h   


    def game_over(self, force_recalculate = False):
        """
        Return true if game is over (win or draw)
        also set winner and ended
        """
        if not force_recalculate and self.ended:
            return self.ended

        # check rows
        for i in range(LENGTH):
            for player in (self.x, self.o):
                if self.board[i].sum() == player*LENGTH:
                    self.winner = player
                    self.ended = True
                    return True
        # check columns
        for j in range(LENGTH):
            for player in (self.x, self.o):
                if self.board[:, j].sum() == player*LENGTH:
                    self.winner = player
                    self.ended = True
                    return True
        # check diagonals
        for player in (self.x, self.o):
            if self.board.trace() == player*LENGTH:
                self.winner = player
                self.ended = True
                return True

            if np.fliplr(self.board).trace() == player*LENGTH:
                self.winner = player
                self.ended = True
                return True

        # check draw
        if np.all((self.board == 0) == False):
            self.winner = None
            self.ended = True
            return True

        # game not over
        self.winner = None
        return False

    def is_draw(self):
        return self.ended and self.winner is None

    
  # -------------
  # | x |   |   |
  # -------------
  # |   |   |   |
  # -------------
  # |   |   | o |
  # -------------

    def draw_board(self):
        for i in range(LENGTH):
            print("-------------")
            for j in range(LENGTH):
                print("  ", end = "")
                if self.board[i,j] == self.x:
                    print("x ", end="")
                elif self.board[i,j] == self.o:
                     print("o ", end="")
                else:
                    print("  ", end="")
            print("")
        print("-------------")
        