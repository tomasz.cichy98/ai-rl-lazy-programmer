import numpy as np
import matplotlib.pyplot as plt
from tkinter import *
import tkinter.messagebox
import time

from agent import Agent
from environment import Environment
from human import Human
from agent_minimax import Agent_Minimax

LENGTH = 3
T = 10000
tk = Tk()

def get_state_hash_and_winner(env, i = 0, j = 0):
    """
    Recursive, will return all possible states as integers
    and who the corresponding winner is. 
    Impossible games are ignored
    """
    results = []

    for v in (0, env.x, env.o):
        env.board[i,j] = v
        if j == 2:          # go back to 0 and next line
            if i == 2:      # the board is full
                state = env.get_state()
                ended = env.game_over(force_recalculate = True)
                winner = env.winner
                results.append((state, winner, ended))
            else:
                results += get_state_hash_and_winner(env, i+1,0)
        else:
            results += get_state_hash_and_winner(env, i, j+1)

        return results

def initial_V_x(env, state_winner_triples):
    """
    init state values
    if x wins V(s) = 1
    if x loses = -1
    draw V(s) = 0
    otherwise V(s) = 0
    """

    V = np.zeros(env.num_states)
    for state, winner, ended in state_winner_triples:
        if ended:
            if winner == env.x:
                v = 1
            elif winner == env.o:
                v = -1
            else:
                v = 0
        else:
            v = 0
        V[state] = v
    return V

def initial_V_o(env, state_winner_triples):
    """
    init state values
    if o wins V(s) = 1
    if o loses = -1
    of draw V(s) = 0
    otherwise V(s) = 0
    """

    V = np.zeros(env.num_states)
    for state, winner, ended in state_winner_triples:
        if ended:
            if winner == env.o:
                v = 1
            elif winner == env.x:
                v = -1
            else:
                v = 0
        else:
            v = 0
        V[state] = v
    return V

def play_game(p1, p2, env, t, draw = False):
    current_player = None

    while not env.game_over():
        if current_player == p1:
            current_player = p2
        else:
            current_player = p1

        if draw:
            if draw == 1 and current_player == p1:
                env.draw_board()
            if draw == 2 and current_player == p2:
                env.draw_board()

        # make move
        current_player.take_action(env, t)

        # update states hist
        state = env.get_state()
        p1.update_state_history(state)
        p2.update_state_history(state)


    if draw:
        env.draw_board()
        if env.winner == env.x:
            print("Congratulations x")
        elif env.winner == env.o:
            print("Congratulations o")
        elif env.winner == None:
            print("DRAW")
    # value function update
    p1.update(env)
    p2.update(env)

def btnClick(buttons, env, i, j, p1, p2):
    global tk
    env.board[i, j] = 1    
    matrix_to_button(env, buttons)    
    # updates after human move
    update_state_and_V(env,p1,p2)

    # check for winning move
    if env.game_over(force_recalculate=True):
        # game_over_animation(buttons)
        matrix_to_button(env, buttons)
        disable_buttons(buttons)        
        # env.draw_board()        
        tk.after(2500, reset_game, env, buttons, p1)
        return

    # ai move
    current_player = p1
    current_player.take_action(env, T, is_gui = True)
    matrix_to_button(env, buttons)
    update_state_and_V(env, p1, p2)

    # check for winning move
    if env.game_over(force_recalculate=True):
        # game_over_animation(buttons)
        matrix_to_button(env, buttons)
        disable_buttons(buttons)        
        # env.draw_board()        
        tk.after(2500, reset_game, env, buttons, p1)
        return

def update_state_and_V(env, p1,p2):
     # update states hist
    state = env.get_state()
    p1.update_state_history(state)
    p2.update_state_history(state)

    # value function update
    p1.update(env)
    p2.update(env)

def disable_buttons(buttons):
    for i in range(LENGTH):
        for j in range(LENGTH):
            buttons[(i,j)]["state"] = "disabled"

def matrix_to_button(env, buttons):
    board = env.board

    for i in range(LENGTH):
        for j in range(LENGTH):
            if board[i, j] == 0:
                buttons[(i,j)]["text"] = " "
            elif board[i, j] == 1:
                buttons[(i,j)]["text"] = "o"
                buttons[(i,j)]["bg"] = "pink"
            elif board[i, j] == -1:
                buttons[(i,j)]["text"] = "x"
                buttons[(i,j)]["bg"] = "powder blue"

def game_over_animation(buttons):
    for i in range(LENGTH):
        for j in range(LENGTH):
            buttons[(i,j)]["bg"] = "gold"

def reset_game(env, buttons, p1):
    tk.quit()
    print(f"Winner is {env.winner}")
    # start next game
    human = Human()
    human.set_symbol(env.o)
    play_game_gui(p1, human, Environment())

def play_game_gui(p1, p2, env):
    global tk
    current_player = p1
    # tk = Tk()
    tk.title("Super Smart Tic Tac Toe")

    player1_name = Entry(tk, textvariable="AI", bd=5)
    player1_name.grid(row=1, column=1, columnspan=8)
    player2_name = Entry(tk, textvariable="Human", bd=5)
    player2_name.grid(row=2, column=1, columnspan=8)

    label1 = Label( tk, text="Player 1:", font='Times 20 bold', bg='white', fg='black', height=1, width=8)
    label1.grid(row=1, column=0)

    label2 = Label( tk, text="Player 2:", font='Times 20 bold', bg='white', fg='black', height=1, width=8)
    label2.grid(row=2, column=0)

    # generate buttons
    buttons = {}
    for i in range(LENGTH):
        for j in range(LENGTH):
            row, col = i + 3, j
            buttons[(i,j)] = Button(tk, text=" ", font='Times 20 bold',
                                    bg='gray', fg='white', height=4, width=8,
                                    command = lambda i = i, j = j: btnClick(buttons, env, i ,j, p1, p2))
            buttons[(i,j)].grid(row = row, column = col)

    current_player.take_action(env, T, is_gui = True)
    # print(f"Agent's epsilon: {current_player.eps}")
    matrix_to_button(env, buttons)

    tk.mainloop()
    
def setup_train_and_play(is_minimax = True):
    # init V
    env = Environment()
    state_winner_triples = get_state_hash_and_winner(env)

    if not is_minimax:
        # create agents
        p1 = Agent()
        p2 = Agent()

        Vx = initial_V_x(env, state_winner_triples)
        p1.set_V(Vx)
        Vo = initial_V_o(env, state_winner_triples)
        p2.set_V(Vo)

        # give symbol
        p1.set_symbol(env.x)
        p2.set_symbol(env.o)

        # T = 1000
        # NOTE: change T to 10000 for actural training, 1000 for debuging
        print(f"Training agents for {T} episodes.")
        for t in range(T):
            if t%200 == 0:
                print(t)
            play_game(p1, p2, Environment(), t)
        p1.plot_epsilon()
        # play human vs agent
        human = Human()
        human.set_symbol(env.o)
    else:
        # play human vs agent
        human = Human()
        human.set_symbol(env.o)
        p1 = Agent_Minimax()
        p1.set_symbol(env.x)

    while True:
        p1.set_verbose(True)
        # play_game(p1, human, Environment(), draw = 2)
        play_game_gui(p1, human, Environment())
        answer = input("Play again? [Y/n]: ")
        if answer and answer.lower()[0] == 'n':
            break

if __name__ == "__main__":
    setup_train_and_play()
