import numpy as np

LENGTH = 3

class Human:
    def __init__(self):
        pass

    def set_symbol(self, sym):
        self.sym = sym
    
    def take_action(self, env, is_gui=False):
        if not is_gui:
            while True:
                # break after move
                move = input("Enter coordinates (i,j) in {0,1,2}: ")
                i, j = move.split(",")
                i, j = int(i), int(j)
                if env.is_empty(i, j):
                    env.board[i,j] = self.sym
                    break
        else:
            pass
    
    def take_action_gui(self, env):
        pass
    
    def update(self, env):
        pass

    def update_state_history(self, s):
        pass
