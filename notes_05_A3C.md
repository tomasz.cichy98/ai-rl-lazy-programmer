# Asynchronous Advantage Actor-Critic
No new theory. Only a coding challange.
- Actor - Policy
- Critic - Value
- Advantage = G - V(s)

## Policy Gradients
2 neural networks:

```math
\pi(a\;|\;s,\;\theta_p) = NeuralNet(input:\;s, \; weights:\; \theta_p)
```
```math
V(s,\; \theta_v) = NeuralNet(input:\;s, \; weights:\; \theta_v)
```
For the policy loss, work back from policy gradient:
```math
L_p = -(G-V(s))\log\pi(a\;|\;s,\; \theta_p)
```
```math
L_v = (G-V(s,\;\theta_v))^2
```

## Entropy
To encourage exploration add entropy to policy loss:
```math
H = -\sum_{k=1}^K\pi_k\log\pi_k
```
The new loss is:
```math
Lp' = Lp + CH
```

## A3C Algorithm
```mermaid
graph TD
	A[Global Networks] -->|Copy params| B(Worker 1)
	A[Global Networks] -->|Copy params| C(Worker 2)
	A[Global Networks] -->|Copy params| D(Worker 3)
	A[Global Networks] -->|Copy params| E(Worker n)
```
First Workers copy the parameters of the global networks and then play episodes using them and send back gradients.  
Global master network has no work to do -  the workers do all the work. Global network gets many updates in parallel.
### Stability
- Each episode will progress randomly. Each action is sampled probabilistically
- Similar to using batch gradient descent
- DQNs use experience replay to look at multiple examples per step.
- A3C uses multiple parallel agents

## Code structure
- main.py (master file)
	- Create and coordinate workers
- worker.py (local policy and value)
	- Copy weights from global nets
	- Play episodes
	- Send gradients back to master
- nets.py
	- Definition of policy and value networks
