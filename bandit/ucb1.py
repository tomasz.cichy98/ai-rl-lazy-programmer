import numpy as np
import matplotlib.pyplot as plt
from eps_greedy import run_experiment as run_experiment_eps

class Bandit:
    def __init__(self, m):
        self.m = m
        self.mean = 0
        self.N = 0
    
    def pull(self):
        return np.random.randn() + self.m   # random from normal with mean 0 sd 1

    def update(self, x):
        self.N += 1
        self.mean = (1- 1.0/self.N)*self.mean + 1.0/self.N*x

def ucb(mean, n, nj):
    """
    n   : number of total plays
    nj  : number of j-th bandit plays
    mean: mean of the j-th bandit
    """
    if nj == 0:
        return float("inf")
    return mean + np.sqrt(2*np.log(n)/ nj)


def run_experiment(m1, m2, m3, N):
    bandits = [Bandit(m1), Bandit(m2), Bandit(m3)]

    data = np.empty(N)

    for i in range(N):
        # UCB1; no eps greedy
        j = np.argmax([ucb(b.mean, i+1, b.N) for b in bandits])

        x = bandits[j].pull()
        bandits[j].update(x)

        data[i] = x
    cumul_mean = np.cumsum(data) / (np.arange(N) + 1)

    # plots
    plt.plot(cumul_mean)
    plt.plot(np.ones(N)*m1)
    plt.plot(np.ones(N)*m2)
    plt.plot(np.ones(N)*m3)
    plt.xscale('log')
    plt.title(f"Cumulative mean for UCB1 experiment")
    plt.xlabel("N")
    plt.ylabel("Cumul mean")
    plt.show()

    for b in bandits:
        print(f"(UCB1) Calculated mean for the bandit with m = {b.m} : {b.mean}")
    
    return cumul_mean

if __name__ == "__main__":
    c_1 = run_experiment_eps(1.0, 2.0, 3.0, 0.1, 10000)
    oiv = run_experiment(1.0, 2.0, 3.0, 10000)

    # plot
    plt.plot(c_1, label = "eps = 0.1")
    plt.plot(oiv, label = "optimistic")
    plt.legend()
    plt.xscale('log')
    plt.title(f"Cumulative mean for all experiments")
    plt.xlabel("N")
    plt.ylabel("Cumul mean")
    plt.show()