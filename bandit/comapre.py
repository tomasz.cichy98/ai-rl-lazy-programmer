import numpy as np
import matplotlib.pyplot as plt
from eps_greedy import Bandit
from optimistic_initial_vals import run_experiment as run_experiment_oiv
from ucb1 import run_experiment as run_experiment_ucb


class BayesianBandit:
    def __init__(self, true_mean):
        self.true_mean = true_mean
        self.predicted_mean = 0
        self.lambda_ = 1
        self.sum_x = 0
        self.tau = 1

    def pull(self):
        """
        Give random outcome for the distr
        """
        return np.random.randn() + self.true_mean

    def sample(self):
        return np.random.randn()/np.sqrt(self.lambda_) + self.predicted_mean

    def update(self, x):
        self.lambda_ += self.tau
        self.sum_x += x
        self.predicted_mean = self.tau*self.sum_x/self.lambda_

def run_experiment_decaying_epsilon(m1, m2, m3, N):
    bandits = [Bandit(m1), Bandit(m2), Bandit(m3)]
    data = np.empty(N)

    for i in range(N):
        # eps greedy
        p = np.random.random()
        if p < 1.0/(i+1):
            j = np.random.choice(3)
        else:
            j = np.argmax([b.mean for b in bandits])
        x = bandits[j].pull()
        bandits[j].update(x)
    
        data[i] = x
    cumul_mean = np.cumsum(data)/ (np.arange(N) + 1)

    # plots
    plt.plot(cumul_mean)
    plt.plot(np.ones(N)*m1)
    plt.plot(np.ones(N)*m2)
    plt.plot(np.ones(N)*m3)
    plt.xscale('log')
    plt.title(f"Cumulative mean for experiment w/ decaying eps")
    plt.xlabel("N")
    plt.ylabel("Cumul mean")
    plt.show()

    return cumul_mean

def run_experiment_bayes(m1, m2, m3, N):
    bandits = [BayesianBandit(m1), BayesianBandit(m2), BayesianBandit(m3)]

    data = np.empty(N)

    for i in range(N):
        # Thompson sampling
        j = np.argmax([b.sample() for b in bandits])

        x = bandits[j].pull()
        bandits[j].update(x)

        data[i] = x
    cumul_mean = np.cumsum(data) / (np.arange(N) + 1)

    # plots
    plt.plot(cumul_mean)
    plt.plot(np.ones(N)*m1)
    plt.plot(np.ones(N)*m2)
    plt.plot(np.ones(N)*m3)
    plt.xscale('log')
    plt.title(f"Cumulative mean for Bayesian Sampling experiment")
    plt.xlabel("N")
    plt.ylabel("Cumul mean")
    plt.show()

    for b in bandits:
        print(f"Calculated mean for the bayes bandit with m = {b.true_mean} : {b.predicted_mean}")
    
    return cumul_mean

if __name__ == "__main__":
    m1, m2, m3 = 1.0, 2.0, 3.0
    N = 10000
    eps = run_experiment_decaying_epsilon(m1,m2,m3,N)
    oiv = run_experiment_oiv(m1,m2,m3,N)
    ucb = run_experiment_ucb(m1,m2,m3,N)
    bayes = run_experiment_bayes(m1,m2,m3,N)
    
    # plot
    plt.plot(eps, label = "decaying epsilon greedy")
    plt.plot(oiv, label = "optimistic")
    plt.plot(ucb, label = "UCB1")
    plt.plot(bayes, label = "bayesian")
    plt.legend()
    plt.xscale('log')
    plt.title("Cumulative mean for all experiments (log)")
    plt.xlabel("N")
    plt.ylabel("Cumul mean")
    plt.show()


        # plot
    plt.plot(eps, label = "decaying epsilon greedy")
    plt.plot(oiv, label = "optimistic")
    plt.plot(ucb, label = "UCB1")
    plt.plot(bayes, label = "bayesian")
    plt.legend()
    plt.xscale('log')
    plt.title("Cumulative mean for all experiments (lin)")
    plt.xlabel("N")
    plt.ylabel("Cumul mean")
    plt.show()