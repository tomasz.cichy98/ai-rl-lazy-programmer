# Artificial Intelligence: Reinforcement Learning in Python
Coding material from [Artificial Intelligence: Reinforcement Learning in Python](https://www.udemy.com/course/artificial-intelligence-reinforcement-learning-in-python/learn/lecture/6431546#overview) (Part 1), and [Advanced AI: Deep Reinforcement Learning in Python](https://www.udemy.com/course/deep-reinforcement-learning-in-python/) (Part 2).
Notes:
- [Part 1](./notes_01_MDPs.md) [MDPs]
- [Part 2](./notes_02_deepRL.md) [RBF and TD($`\lambda`$)]
- [Part 3](./notes_03_policy_gradients.md) [Policy Gradients]
- [Part 4](./notes_04_Deep-Q.md) [Deep Q-Learning]
- [Part 5](./notes_05_A3C.md) [A3C]

## Course 1
Folders:
- bandit
- grid_world
- tic-tac-toe

There is also a lot of stuff not covered in the course such as Tic-Tac-Toe GUI and MiniMax or big grids and grid action generator.

## Course 2
Folders:
- gym
- a3c (also on [Kaggle](https://www.kaggle.com/pantomasz98/a3c-tf1-15))

Colabs:
- [Double Deep Q-Learning Cartpole](https://colab.research.google.com/drive/1bsfBlIF6gs-miNWO-fT9pIZzIDgHIF0R)
- [Double Deep Q-Learning Breakout](https://colab.research.google.com/drive/1cWbHi_y3H_o3St3CcQefsAAmLXI8jdBV)

### A3C Videos and Plots
As per the original paper, nothing interesting will happen for any of the games using only 4 threads for only 8 hours. Kaggle notebook to run all the files can be found [here](https://www.kaggle.com/pantomasz98/a3c-tf1-15).

![Asynchronous Methods for Deep Reinforcement Learning](./media/plots/orig_perf.png "https://arxiv.org/pdf/1602.01783.pdf")
#### [Breakout-v0](https://gym.openai.com/envs/Breakout-v0/) - 8.5 hours of training
![worker-0](./media/gym_vids/Breakout/openaigym.video.0.13.video000000.gif "worker-0")
![worker-1](./media/gym_vids/Breakout/openaigym.video.1.13.video000000.gif "worker-1")
![worker-2](./media/gym_vids/Breakout/openaigym.video.2.13.video000000.gif "worker-2")
![worker-3](./media/gym_vids/Breakout/openaigym.video.3.13.video000000.gif "worker-3")
![plot_breakout](./media/plots/plot_breakout.png "Breakout_plot")

#### [BeamRider-v0](https://gym.openai.com/envs/BeamRider-v0/) - 8.5 hours of training
![worker-0](./media/gym_vids/BeamRider/openaigym.video.0.14.video000000.gif "worker-0")
![worker-1](./media/gym_vids/BeamRider/openaigym.video.1.14.video000000.gif "worker-1")
![worker-2](./media/gym_vids/BeamRider/openaigym.video.2.14.video000000.gif "worker-2")
![worker-3](./media/gym_vids/BeamRider/openaigym.video.3.14.video000000.gif "worker-3")
![plot_beam](./media/plots/plot_beam.png "BeamRider_plot")

#### [SpaceInvaders-v0](https://gym.openai.com/envs/SpaceInvaders-v0/) - 8.5 hours of training
![worker-0](./media/gym_vids/SpaceInvaders/openaigym.video.0.14.video000000.gif "worker-0")
![worker-1](./media/gym_vids/SpaceInvaders/openaigym.video.1.14.video000000.gif "worker-1")
![worker-2](./media/gym_vids/SpaceInvaders/openaigym.video.2.14.video000000.gif "worker-2")
![worker-3](./media/gym_vids/SpaceInvaders/openaigym.video.3.14.video000000.gif "worker-3")
![plot_space](./media/plots/plot_space.png "SpaceInvaders_plot")

#### [PongDeterministic-v4](https://gym.openai.com/envs/PongDeterministic-v4) - 8.5 hours of training
![worker-0](./media/gym_vids/PongDeterministic-v4/openaigym.video.0.14.video000000.gif "worker-0")
![worker-1](./media/gym_vids/PongDeterministic-v4/openaigym.video.1.14.video000000.gif "worker-1")
![worker-2](./media/gym_vids/PongDeterministic-v4/openaigym.video.2.14.video000000.gif "worker-2")
![worker-3](./media/gym_vids/PongDeterministic-v4/openaigym.video.3.14.video000000.gif "worker-3")
![plot_pong_deter](./media/plots/plot_pong_deter.png "PongDeterministic_plot")

#### [BreakoutDeterministic-v4](https://gym.openai.com/envs/BreakoutDeterministic-v4) - 8.5 hours of training
![worker-0](./media/gym_vids/BreakoutDeterministic-v4/openaigym.video.0.13.video000000.gif "worker-0")
![worker-1](./media/gym_vids/BreakoutDeterministic-v4/openaigym.video.1.13.video000000.gif "worker-1")
![worker-2](./media/gym_vids/BreakoutDeterministic-v4/openaigym.video.2.13.video000000.gif "worker-2")
![worker-3](./media/gym_vids/BreakoutDeterministic-v4/openaigym.video.3.13.video000000.gif "worker-3")
![plot_break_deter](./media/plots/plot_break_deter.png "BreakoutDeterministic_plot")

#### [BeamRiderDeterministic-v4](https://gym.openai.com/envs/BeamRiderDeterministic-v4) - 8.5 hours of training
![worker-0](./media/gym_vids/BeamRiderDeterministic-v4/openaigym.video.0.14.video000000.gif "worker-0")
![worker-1](./media/gym_vids/BeamRiderDeterministic-v4/openaigym.video.1.14.video000000.gif "worker-1")
![worker-2](./media/gym_vids/BeamRiderDeterministic-v4/openaigym.video.2.14.video000000.gif "worker-2")
![worker-3](./media/gym_vids/BeamRiderDeterministic-v4/openaigym.video.3.14.video000000.gif "worker-3")
![plot_beam_deter](./media/plots/plot_beam_deter.png "BeamRiderDeterministic_plot")

## Links
- http://www.ecmlpkdd2013.org/wp-content/uploads/2013/09/Multiagent-Reinforcement-Learning.pdf [Handouts MARL]
- https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw [NN Design]
- http://www.faqs.org/faqs/ai-faq/neural-nets/part1/preamble.html [NN Design]
- https://mermaid-js.github.io/mermaid-live-editor [Mermaid Live Editor]
- https://arxiv.org/abs/1602.01783 [A3C]
- https://gym.openai.com/envs/Breakout-v0/ [Gym Breakout]
- https://gym.openai.com/envs/BeamRider-v0/ [Gym BeamRider]
- https://gym.openai.com/envs/Tennis-v0/ [Gym Tennis]
- https://gym.openai.com/envs/Qbert-v0/ [Gym Qbert]
- https://gym.openai.com/envs/Pong-v0/ [Gym Pong]
