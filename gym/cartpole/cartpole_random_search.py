import gym
from gym import wrappers
import numpy as np
import matplotlib.pyplot as plt

def get_action(s, w):
    # 0: push left
    # 1: push right
    return 1 if s.dot(w) > 0 else 0

def play_one_episode(env, params, verbose = False):
    state = env.reset()
    done = False
    t = 0

    while not done and t < 10000:
        if verbose:
            env.render()
        t += 1
        action = get_action(state, params)
        state, reward, done, info = env.step(action)
        if done:
            break

    return t

def play_multiple_episodes(env, T, params, verbose = False):
    episode_lengths = np.empty(T)

    for i in range(T):
        episode_lengths[i] = play_one_episode(env, params, verbose)

    avg_len = episode_lengths.mean()
    print("avg len:", avg_len)
    return avg_len

def random_search(env):
    episode_lengths = []
    best = 0
    params = None

    for t in range(100):
        new_params = np.random.random(4)*2 - 1
        avg_len = play_multiple_episodes(env, 100, new_params)
        episode_lengths.append(avg_len)

        if avg_len > best:
            params = new_params
            best = avg_len
    return episode_lengths, params


if __name__ == "__main__":
    env = gym.make('CartPole-v0')
    episode_lengths, params = random_search(env)
    
    plt.plot(episode_lengths)
    plt.xlabel('t')
    plt.ylabel('avg episode length')
    plt.show()

    # save a video
    env = wrappers.Monitor(env, "./gym_videos/", force = True)

    # play final set of episdes
    print("Using final weights:")
    play_multiple_episodes(env, 2, params)