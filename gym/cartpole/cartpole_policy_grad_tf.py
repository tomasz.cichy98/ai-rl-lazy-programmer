import gym
import os
import sys
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from gym import wrappers
from datetime import datetime
from typing import List
from cartpole_q_learning_bins import plot_running_avg

CURRENT_DIR = os.path.dirname(__file__)
SAVE_PATH = CURRENT_DIR + "/saves/q_learning_models.pickle"
VIDEO_SAVE_PATH = CURRENT_DIR + "/gym_videos/"

tf.random.set_seed(123)
np.random.seed(123)

class PolicyModel:
    def __init__(self, input_size, output_size, hidden_layer_sizes):
        input_ = tf.keras.layers.Input(input_size)
        self.output_size = output_size
        net = input_
        # build hidden layers
        for hidden_layer_size in hidden_layer_sizes:
            net = tf.keras.layers.Dense(hidden_layer_size, activation = "tanh")(net)
        # add output
        net = tf.keras.layers.Dense(output_size, activation = "softmax", use_bias = False)(net)
        # build a model
        self.model = tf.keras.models.Model(input_, net)
        self.model.build(input_size)

        self.optimizer = tf.keras.optimizers.Adagrad(10e-2)
        self.do_minimize = tf.function(self.minimize, input_signature = [
            tf.TensorSpec(shape = (None, input_size), dtype = tf.float32),  # input
            tf.TensorSpec(shape = None, dtype = tf.int32),                  # actions
            tf.TensorSpec(shape = None, dtype = tf.float32)                 # advanta
        ])

    @tf.function
    def calculate_forward(self, input_data):
        return self.model(input_data)

    def minimize(self, input_data, actions, advantages):
        # cost must be a function
        def calc_cost():
            # max sum[ (G-V)log pi(a | s) ]
            # min - sum[ (G-V)log pi(a | s) ]
            p_a_given_s = self.model(input_data)
            selected_probs = tf.math.log(tf.reduce_sum(p_a_given_s * tf.one_hot(actions, self.output_size), 1))
            return -tf.reduce_sum(advantages * selected_probs)
        self.optimizer.minimize(calc_cost, self.model.trainable_variables)

    def partial_fit(self, input_data, actions, advantages):
        input_data = np.atleast_2d(input_data)
        actions = np.atleast_1d(actions)
        advantages = np.atleast_1d(advantages)
        self.do_minimize(input_data, actions, advantages)

    def predict(self, input_data):
        input_data = np.atleast_2d(input_data)
        return self.calculate_forward(input_data).numpy()

    def sample_action(self, input_data):
        p = self.predict(input_data)[0]
        # p = [0.20, 0.20, 0.60] for example
        # choose a number from 0, 1, 2 with prob equal to the element in p
        # so we get 0:20%, 1:20% and 2:60%
        return np.random.choice(len(p), p = p)

class ValueModel:
    def __init__(self, input_size: int, hidden_layer_sizes: List[int]):
        input_ = tf.keras.layers.Input(input_size)
        net = input_
        # build hidden layers
        for hidden_layer_size in hidden_layer_sizes:
            net = tf.keras.layers.Dense(hidden_layer_size, activation = "tanh")(net)
        # add output
        net = tf.keras.layers.Dense(1)(net)
        # build a model
        self.model = tf.keras.models.Model(input_, net)
        self.model.build(input_size)

        self.optimizer = tf.keras.optimizers.SGD(10e-5)
        self.do_minimize = tf.function(self.minimize, input_signature = [
            tf.TensorSpec(shape = (None, input_size), dtype = tf.float32),  # input
            tf.TensorSpec(shape = None, dtype = tf.float32)                 # target
        ])

    def minimize(self, input_data, target_data):
        # cost must be a function
        def calc_cost():
            y_hat = tf.reshape(self.model(input_data), [-1])
            return tf.reduce_sum(tf.math.square(target_data - y_hat))
        self.optimizer.minimize(calc_cost, self.model.trainable_variables)

    def partial_fit(self, input_data, target_data):
        input_data = np.atleast_2d(input_data)
        target_data = np.atleast_2d(target_data)
        self.do_minimize(input_data, target_data)

    @tf.function
    def calculate_forward(self, input_data):
        return self.model(input_data)    

    def predict(self, input_data):
        input_data = np.atleast_2d(input_data)
        return self.calculate_forward(input_data).numpy()


def play_one_mc(env, pmodel, vmodel, gamma, render = False):
    observation = env.reset()
    done = False
    total_reward = 0
    iters = 0

    states = []
    actions = []
    rewards = []

    reward = 0
    # i will set the iter cap to be 1000 not 200
    while not done and iters < 1000:
        if render:
            env.render()
        action = pmodel.sample_action(observation)

        states.append(observation)
        actions.append(action)
        rewards.append(reward)

        prev_observation = observation
        observation, reward, done, info = env.step(action)

        if done and iters < 999:
            reward = -200
        
        # if reward == 1:
        total_reward += reward

        iters += 1

    # save final (s, a, r)
    action = pmodel.sample_action(observation)
    states.append(observation)
    actions.append(action)
    rewards.append(reward)

    returns = []
    advantages = []

    G = 0
    for s, r in zip(reversed(states), reversed(rewards)):
        returns.append(G)
        advantages.append(G - vmodel.predict(s)[0])
        G = r + gamma*G
    returns.reverse()
    advantages.reverse()

    # update the model
    pmodel.partial_fit(states, actions, advantages)
    vmodel.partial_fit(states, returns)

    return total_reward


def main():
    env = gym.make('CartPole-v0')
    env._max_episode_steps = 1000
    D = env.observation_space.shape[0]
    K = env.action_space.n
    pmodel = PolicyModel(D, K, [5])
    vmodel = ValueModel(D, [10])
    # init = tf.gl
    # session = 
    gamma = 0.99

    N = 1000
    total_rewards = np.empty(N)
    costs = np.empty(N)
    for n in range(N):
        total_reward = play_one_mc(env, pmodel, vmodel, gamma)
        total_rewards[n] = total_reward
        if n%10 == 0:
            print("episode:", n, "total reward:", total_reward, "avg reward (last 100):", total_rewards[max(0, n-100):(n+1)].mean())
    print("avg reward for last 100 episodes:", total_rewards[-100:].mean())
    print("total steps:", total_rewards.sum())

    plt.plot(total_rewards)
    plt.title("Rewards")
    plt.xlabel("n")
    plt.show()

    plot_running_avg(total_rewards)

    tf.keras.utils.plot_model(pmodel.model, CURRENT_DIR + "/pmodel.png", show_shapes = True, expand_nested=False)
    tf.keras.utils.plot_model(vmodel.model, CURRENT_DIR + "/vmodel.png", show_shapes = True, expand_nested=False)

    env = wrappers.Monitor(env, VIDEO_SAVE_PATH, force = True)
    play_one_mc(env, pmodel, vmodel, gamma, True)


if __name__ == '__main__':
  main()