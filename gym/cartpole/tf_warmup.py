import numpy as np
import tensorflow as tf
import cartpole_q_learning

tf.compat.v1.disable_eager_execution()
tf.compat.v1.disable_v2_behavior()

# the lesson was made using tf 1.0 and I use ft 2.1
# I have used the docs to figure out the differences

class SGDRegressor:
    def __init__(self, D):
        print("Hello TensorFlow 2.1")
        lr = 0.1
        
        # create inputs, targets and params
        self.w = tf.Variable(tf.random.normal(shape = (D, 1)), name = "w")
        self.X = tf.keras.backend.placeholder(dtype = tf.float32, shape = (None, D), name = "X")
        self.Y = tf.keras.backend.placeholder(dtype = tf.float32, shape = (None, ), name = "Y")

        # make prediction and cost
        Y_hat = tf.reshape(tf.matmul(self.X, self.w), [-1])
        delta = self.Y - Y_hat
        cost = tf.reduce_sum(delta*delta)

        # ops
        self.predict_op = Y_hat

        # self.opt = tf.keras.optimizers.SGD(lr)
        # self.train_op = self.opt.minimize(loss = cost, var_list = [self.predict_op])

        self.train_op = tf.compat.v1.train.GradientDescentOptimizer(lr).minimize(cost)

        # start and init
        init = tf.compat.v1.global_variables_initializer()
        self.session = tf.compat.v1.InteractiveSession()
        self.session.run(init)

    def partial_fit(self, X, Y):
        self.session.run(self.train_op, feed_dict = {self.X: X, self.Y: Y})

    def predict(self, X):
        return self.session.run(self.predict_op, feed_dict = {self.X: X})

if __name__ == "__main__":
    cartpole_q_learning.SGDRegressor = SGDRegressor
    cartpole_q_learning.main(N = 200)