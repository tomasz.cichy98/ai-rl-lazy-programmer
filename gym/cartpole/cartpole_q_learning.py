import gym
import os
import sys
import pickle
import numpy as np
import matplotlib.pyplot as plt
from gym import wrappers
from datetime import datetime
from sklearn.pipeline import FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.kernel_approximation import RBFSampler
from cartpole_q_learning_bins import plot_running_avg

CURRENT_DIR = os.path.dirname(__file__)
SAVE_PATH = CURRENT_DIR + "/saves/q_learning_models.pickle"
VIDEO_SAVE_PATH = CURRENT_DIR + "/../gym_videos/"

class SGDRegressor:
    def __init__(self, D):
        self.w = np.random.randn(D) / np.sqrt(D)
        self.lr = 0.01

    def partial_fit(self, X, Y):
        self.w += self.lr*(Y-X.dot(self.w)).dot(X)
        
    def predict(self, X):
        return X.dot(self.w)


class FeatureTransformer:
    def __init__(self, env):
        observation_examples = np.random.random((2000, 4))*2 - 1
        scaler = StandardScaler()
        scaler.fit(observation_examples)

        featuriser = FeatureUnion([
            ("rbf1", RBFSampler(gamma=0.05, n_components=1000)),
            ("rbf2", RBFSampler(gamma=1.0, n_components=1000)),
            ("rbf3", RBFSampler(gamma=0.5, n_components=1000)),
            ("rbf4", RBFSampler(gamma=0.1, n_components=1000))
        ])

        feature_examples = featuriser.fit_transform(scaler.transform(observation_examples))

        self.dimensions = feature_examples.shape[1]
        self.scaler = scaler
        self.featuriser = featuriser

    def transform(self, observation):
        scaled = self.scaler.transform(observation)
        return self.featuriser.transform(scaled)

class Model:
    def __init__(self, env, feature_transformer):
        self.env = env
        self.models = []
        self.feature_transformer = feature_transformer
        # init regressors
        for i in range(env.action_space.n):
            model = SGDRegressor(feature_transformer.dimensions)
            self.models.append(model)

    def predict(self, s):
        X = self.feature_transformer.transform(np.atleast_2d(s))
        result = np.stack([m.predict(X) for m in self.models]).T
        return result

    def update(self, s, a, G):
        X = self.feature_transformer.transform(np.atleast_2d(s))
        self.models[a].partial_fit(X, [G])

    def sample_action(self, s, eps):
        if np.random.random() < eps:
            return self.env.action_space.sample()
        else:
            return np.argmax(self.predict(s))

    def pickle_models(self):
        with open(SAVE_PATH, 'wb') as f:
            pickle.dump(self.models, f)

    def load_pickled_models(self):
        if os.path.exists(SAVE_PATH):
            print("loading pickled models")
            with open(SAVE_PATH, 'rb') as f:
                self.models = pickle.load(f)

def play_one(env, model, eps, gamma, render = False):
    observation = env.reset()
    done = False
    total_reward = 0
    iters = 0

    while not done:
        if render:
            env.render()
        
        action = model.sample_action(observation, eps)
        prev_observation = observation
        observation, reward, done, info = env.step(action)

        if done and iters < 199:
            reward = -200

        # update model
        next = model.predict(observation)
        assert(next.shape == (1, env.action_space.n))
        G = reward + gamma*np.max(next)
        model.update(prev_observation, action, G)

        if reward == 1:
            total_reward += reward
        iters += 1

    return total_reward

def main(N = 500, save_pickle = False, load_pickle = False):
    env = gym.make('CartPole-v0')
    ft = FeatureTransformer(env)
    model = Model(env, ft)
    gamma = 0.99

    if load_pickle:
        model.load_pickled_models()

    total_rewards = np.empty(N)
    costs = np.empty(N)
    
    for n in range(N):
        eps = 1.0/np.sqrt(n+1)
        total_reward = play_one(env, model, eps, gamma)
        total_rewards[n] = total_reward
        if n % 10 == 0:
            print("episode:", n, "total reward:", total_reward, "eps:", eps, "avg reward (last 100):", total_rewards[max(0, n-100):(n+1)].mean())

    print("avg reward for last 100 episodes:", total_rewards[-100:].mean())
    print("total steps:", total_rewards.sum())

    # save
    if save_pickle:
        model.pickle_models()

    # plots
    plt.plot(total_rewards)
    plt.title("Rewards")
    plt.show()

    plot_running_avg(total_rewards)

    # play and save game
    env = wrappers.Monitor(env, VIDEO_SAVE_PATH, force = True)
    play_one(env, model, eps, gamma, True)

if __name__ == '__main__':
    main(load_pickle = True)