import gym
import os
import sys
import pickle
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from gym import wrappers
from datetime import datetime
from sklearn.pipeline import FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.kernel_approximation import RBFSampler
from sklearn.linear_model import SGDRegressor

# SGDRegressor defaults:
# loss='squared_loss', penalty='l2', alpha=0.0001,
# l1_ratio=0.15, fit_intercept=True, n_iter=5, shuffle=True,
# verbose=0, epsilon=0.1, random_state=None, learning_rate='invscaling',
# eta0=0.01, power_t=0.25, warm_start=False, average=False

CURRENT_DIR = os.path.dirname(__file__)
SAVE_PATH = CURRENT_DIR + "/saves/q_learning_models.pickle"
VIDEO_SAVE_PATH = CURRENT_DIR + "/../gym_videos/"

class FeatureTransformer:
    def __init__(self, env, n_components = 500):
        observation_examples = np.array([env.observation_space.sample() for x in range(10000)]) # take 10000 samples from the observation space

        scaler = StandardScaler()
        scaler.fit(observation_examples)

        # We use RBF kernels with different variances to cover different parts of the space
        featuriser = FeatureUnion([
            ("rbf1", RBFSampler(gamma = 5.0, n_components = n_components)),
            ("rbf2", RBFSampler(gamma = 2.0, n_components = n_components)),
            ("rbf3", RBFSampler(gamma = 1.0, n_components = n_components)),
            ("rbf4", RBFSampler(gamma = 0.5, n_components = n_components))
        ])

        example_features = featuriser.fit_transform(scaler.transform(observation_examples))
        
        self.dimensions = example_features.shape[1]
        self.scaler = scaler
        self.featuriser = featuriser

    def transform(self, observation):
        """
        transform a state to a feature vector
        """
        scaled = self.scaler.transform(observation)
        return self.featuriser.transform(scaled)

class Model:
    def __init__(self, env, feature_transformer, learning_rate):
        # self.current_dir = os.path.dirname(__file__)
        # self.SAVE_PATH = self.current_dir + "/saves/q_learning_models.pickle"
        self.env = env
        self.models = []
        self.feature_transformer = feature_transformer

        # each action has it's own model
        for i in range(env.action_space.n):
            model = SGDRegressor(learning_rate = learning_rate)
            # partial fit on start state and reward 0
            model.partial_fit(feature_transformer.transform( [env.reset()] ), [0])
            self.models.append(model)

    def predict(self, s):
        X = self.feature_transformer.transform([s])
        # predict using each model, transpose
        result = np.stack([m.predict(X) for m in self.models]).T

        assert(len(X.shape) == 2)
        return result

    def update(self, s, a, G):
        X = self.feature_transformer.transform([s])
        assert(len(X.shape) == 2)
        self.models[a].partial_fit(X, [G])

    def sample_action(self, s, eps):
        # we do not need to use eps greedy
        # eps = 0

        if np.random.random() < eps:
            return self.env.action_space.sample()
        else:
            return np.argmax(self.predict(s))

    def pickle_models(self):
        with open(SAVE_PATH, 'wb') as f:
            pickle.dump(self.models, f)

    def load_pickled_models(self):
        if os.path.exists(SAVE_PATH):
            print("loading pickled models")
            with open(SAVE_PATH, 'rb') as f:
                self.models = pickle.load(f)

def play_one(model, env, eps, gamma, render = False):
    observation = env.reset()
    done = False
    total_reward = 0

    while not done:
        if render:
            env.render()
        action = model.sample_action(observation, eps)
        prev_observation = observation
        observation, reward, done, info = env.step(action)

        # update mode
        next = model.predict(observation)

        G = reward + gamma*np.max(next[0])
        model.update(prev_observation, action, G)

        total_reward += reward

    return total_reward

def plot_cost_to_go(env, estimator, num_tiles = 20):
    x = np.linspace(env.observation_space.low[0], env.observation_space.high[0], num = num_tiles)
    y = np.linspace(env.observation_space.low[1], env.observation_space.high[1], num = num_tiles)

    X, Y = np.meshgrid(x, y)
    Z = np.apply_along_axis(lambda _: -np.max(estimator.predict(_)), 2, np.dstack([X, Y]))

    fig = plt.figure(figsize=(10, 5))
    ax = fig.add_subplot(111, projection='3d')
    surf = ax.plot_surface(X, Y, Z,
        rstride=1, cstride=1, cmap=matplotlib.cm.coolwarm, vmin=-1.0, vmax=1.0)
    ax.set_xlabel('Position')
    ax.set_ylabel('Velocity')
    ax.set_zlabel('Cost-To-Go == -V(s)')
    ax.set_title("Cost-To-Go Function")
    fig.colorbar(surf)
    plt.show()

def plot_running_avg(total_rewards):
    N = len(total_rewards)
    running_avg = np.empty(N)
    for t in range(N):
        running_avg[t] = total_rewards[max(0, t - 100):(t+1)].mean()

    plt.plot(running_avg)
    plt.title("Running Average")
    plt.xlabel("t")
    plt.ylabel("avg")
    plt.show()

def main(show_plots = True, render = False, load_pickle = False):    
    env = gym.make('MountainCar-v0')
    ft = FeatureTransformer(env)
    model = Model(env, ft, "constant")
    gamma = 0.99

    if load_pickle:
        model.load_pickled_models()

    N = 300
    total_rewards = np.empty(N)
    eps = 0
    for n in range(N):
        # eps = 0.1*(0.97**n)
        total_reward = play_one(model, env, eps, gamma)
        total_rewards[n] = total_reward
        if (n+1)%10 == 0:
            print("episode:", n, "total reward:", total_reward, "eps:", eps)
    print("total steps:", -total_rewards.sum())

    # save
    model.pickle_models()

    if show_plots:
        plt.plot(total_rewards)
        plt.title("Rewards")
        plt.xlabel("t")
        plt.ylabel("reward")
        plt.show()

        plot_running_avg(total_rewards)

        # plot the optimal state-value function
        plot_cost_to_go(env, model)

    env = wrappers.Monitor(env, VIDEO_SAVE_PATH, force = True)
    play_one(model, env, eps, gamma, True)

if __name__ == "__main__":
    main(load_pickle=False)