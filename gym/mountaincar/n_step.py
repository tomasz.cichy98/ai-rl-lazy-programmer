import gym
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from gym import wrappers
from datetime import datetime


# code we already wrote
import mountaincar_q_learning
from mountaincar_q_learning import plot_cost_to_go, FeatureTransformer, Model, plot_running_avg

class SGDRegressor:
    def __init__(self, **kwargs):
        self.w = None
        self.lr = 1e-2

    def partial_fit(self, X, Y):
        if self.w is None:
            D = X.shape[1]
            self.w = np.random.randn(D)/np.sqrt(D)
        self.w += self.lr *(Y - X.dot(self.w)).dot(X)

    def predict(self, X):
        return X.dot(self.w)

# replace sklearn regressor
mountaincar_q_learning.SGDRegressor = SGDRegressor


# calculate everything up to max[Q(s,a)]
# Ex.
# R(t) + gamma*R(t+1) + ... + (gamma^(n-1))*R(t+n-1) + (gamma^n)*max[Q(s(t+n), a(t+n))]
# def calculate_return_before_prediction(rewards, gamma):
#   ret = 0
#   for r in reversed(rewards[1:]):
#     ret += r + gamma*ret
#   ret += rewards[0]
#   return ret

def play_one(model, eps, gamma, n = 5, render = False):
    """
    calculate everything up to max[Q(s,a)]
    Ex.
    R(t) + gamma*R(t+1) + ... + (gamma^(n-1))*R(t+n-1) + (gamma^n)*max[Q(s(t+n), a(t+n))]
    def calculate_return_before_prediction(rewards, gamma):
    ret = 0
    for r in reversed(rewards[1:]):
        ret += r + gamma*ret
    ret += rewards[0]
    return ret
    """

    observation = env.reset()
    done = False
    total_reward = 0
    rewards = []
    states = []
    actions = []
    iters = 0
    
    multiplier = np.array([gamma]*n)**np.arange(n)

    while not done and iters < 200:
        if render:
            env.render()
        action = model.sample_action(observation, eps)

        states.append(observation)
        actions.append(action)

        prev_observation = observation
        observation, reward, done, info = env.step(action)

        rewards.append(reward)

        # update the model
        if len(rewards) >= n:
            return_up_to_prediction = multiplier.dot(rewards[-n:])
            G = return_up_to_prediction + (gamma**n)*np.max(model.predict(observation)[0])
            model.update(states[-n], actions[-n], G)
 
        total_reward += reward
        iters += 1

    # empty
    if n == 1:
        rewards = []
        states = []
        actions = []
    else:
        rewards = rewards[-n+1:]
        states = states[-n+1:]
        actions = actions[-n+1:]

    if observation[0] >= 0.5:
        # made it to the goal
        while len(rewards) > 0:
            G = multiplier[:len(rewards)].dot(rewards)
            model.update(states[0], actions[0], G)
            rewards.pop(0)
            states.pop(0)
            actions.pop(0)
    else:
        # did not make it
        while len(rewards) > 0:
            guess_rewards = rewards + [-1]*(n-len(rewards))
            G = multiplier.dot(guess_rewards)
            model.update(states[0], actions[0], G)
            rewards.pop(0)
            states.pop(0)
            actions.pop(0)

    return total_reward


if __name__ == '__main__':
    env = gym.make('MountainCar-v0')
    ft = FeatureTransformer(env)
    model = Model(env, ft, "constant")
    gamma = 0.99


    N = 100
    total_rewards = np.empty(N)
    costs = np.empty(N)
    for n in range(N):
        # eps = 1.0/(0.1*n+1)
        eps = 0.1*(0.97**n)
        total_reward = play_one(model, eps, gamma)
        total_rewards[n] = total_reward
        print("episode:", n, "total reward:", total_reward)
    print("avg reward for last 100 episodes:", total_rewards[-100:].mean())
    print("total steps:", -total_rewards.sum())

    plt.plot(total_rewards)
    plt.title("Rewards")
    plt.show()

    plot_running_avg(total_rewards)

    # plot the optimal state-value function
    plot_cost_to_go(env, model)

    play_one(env, model, eps, gamma, n = 5 , render = True)