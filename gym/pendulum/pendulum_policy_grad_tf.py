import gym
import os
from datetime import datetime
from typing import List
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.kernel_approximation import RBFSampler
from sklearn.pipeline import FeatureUnion
from sklearn.preprocessing import StandardScaler
# from tensorflow.compat.v1.train import AdamOptimizer
# from tensorflow.keras.optimizers import SGD, Adam
from tensorflow_probability.python.distributions import Normal
from plots import plot_running_avg, plot_cost_to_go

CURRENT_DIR = os.path.dirname(__file__)
SAVE_PATH = CURRENT_DIR + "/saves/q_learning_models.pickle"
VIDEO_SAVE_PATH = CURRENT_DIR + "/gym_videos/"

tf.random.set_seed(123)
np.random.seed(123)

class FeatureTransformer:
    def __init__(self, env, n_components = 100):
        observation_examples = np.array([env.observation_space.sample() for _ in range(10000)])
        scaler = StandardScaler()
        scaler.fit(observation_examples)
        featurizer = FeatureUnion([
            ("rbf1", RBFSampler(gamma = 5.0, n_components = n_components)),
            ("rbf2", RBFSampler(gamma = 2.0, n_components = n_components)),
            ("rbf3", RBFSampler(gamma = 1.0, n_components = n_components)),
            ("rbf4", RBFSampler(gamma = 0.5, n_components = n_components)),
        ])
        example_features = featurizer.fit_transform(scaler.transform(observation_examples))

        self.dimensions = example_features.shape[1]
        self.scaler = scaler
        self.featurizer = featurizer

    def transform(self, observations):
        scaled = self.scaler.transform(observations)
        return self.featurizer.transform(scaled)

class PolicyModel:
    def __init__(self, ft, in_size, hidden_sizes, lr = 1e-3, smooth = 1e-5):
        self.in_size = in_size
        self.hidden_sizes = hidden_sizes
        self.ft = ft
        self.lr = lr
        self.smooth = smooth

        # architecture
        in_l = tf.keras.layers.Input(in_size)
        hl = in_l
        for size in hidden_sizes:
            hl = tf.keras.layers.Dense(size, activation = "tanh")(hl)
        
        # mean out
        out_l_mean = tf.keras.layers.Dense(1, activation = "tanh", use_bias = False, kernel_initializer = "zeros")(hl)
        self.mean_model = tf.keras.models.Model(in_l, out_l_mean)
        self.mean_model.build(in_size)

        # st dev model
        out_l_st_dev = tf.keras.layers.Dense(1, activation = "tanh", use_bias = False)(hl)
        self.st_dev_model = tf.keras.models.Model(in_l, out_l_st_dev)
        self.st_dev_model.build(in_size)

        self.optimizer = tf.keras.optimizers.Adam(lr)
        self.do_minimize = tf.function(self.minimize, input_signature = [
            tf.TensorSpec(shape=(None, in_size), dtype = tf.float32),
            tf.TensorSpec(shape = None, dtype = tf.float32),
            tf.TensorSpec(shape = None, dtype = tf.float32)
        ])

    def minimize(self, states, actions, advantages):
        def loss():
            mean = tf.reshape(self.mean_model(states), [-1])
            st_dev = tf.reshape(self.st_dev_model(states) + self.smooth, [-1])
            norm = Normal(mean, st_dev)
            log_probs = norm.log_prob(actions)
            return -tf.reduce_sum(advantages * log_probs + 0.1 * norm.entropy())
        self.optimizer.minimize(loss, [self.mean_model.trainable_variables, self.st_dev_model.trainable_variables])

    def partial_fit(self, states, actions, advantages):
        states = np.atleast_2d(states)
        actions = np.atleast_1d(actions)
        advantages = np.atleast_1d(advantages)

        features = self.ft.transform(states)
        return self.do_minimize(features, actions, advantages)

    @tf.function
    def __predict_tf(self, state):
        mean = tf.reshape(self.mean_model(state), [-1])
        st_dev = tf.reshape(self.st_dev_model(state) + self.smooth, [-1])
        norm = Normal(mean, st_dev)
        # output must be between -2 nad 2
        return tf.clip_by_value(norm.sample(), -2, 2)

    def predict(self, state):
        state = np.atleast_2d(state)
        features = self.ft.transform(state)
        return self.__predict_tf(features).numpy()[0]

    def sample_action(self, state):
        return self.predict(state)

class ValueModel:
    def __init__(self, ft, input_size: int, hidden_layer_sizes: List[int], lr = 1e-1):
        self.ft = ft
        input_ = tf.keras.layers.Input(input_size)
        net = input_
        # build hidden layers
        for hidden_layer_size in hidden_layer_sizes:
            net = tf.keras.layers.Dense(hidden_layer_size, activation = "tanh")(net)
        # add output
        net = tf.keras.layers.Dense(1)(net)
        # build a model
        self.model = tf.keras.models.Model(input_, net)
        self.model.build(input_size)

        self.optimizer = tf.keras.optimizers.SGD(lr)
        self.do_minimize = tf.function(self.minimize, input_signature = [
            tf.TensorSpec(shape = (None, input_size), dtype = tf.float32),  # input
            tf.TensorSpec(shape = None, dtype = tf.float32)                 # target
        ])

    def minimize(self, input_data, target_data):
        # cost must be a function
        def loss():
            y_hat = tf.reshape(self.model(input_data), [-1])
            return tf.reduce_sum(tf.math.square(target_data - y_hat))
        self.optimizer.minimize(loss, self.model.trainable_variables)

    def partial_fit(self, input_data, target_data):
        input_data = np.atleast_2d(input_data)
        target_data = np.atleast_2d(target_data)
        features = self.ft.transform(input_data)
        self.do_minimize(features, target_data)

    @tf.function
    def __predict_tf(self, input_data):
        return self.model(input_data)    

    def predict(self, input_data):
        input_data = np.atleast_2d(input_data)
        features = self.ft.transform(input_data)
        return self.__predict_tf(features).numpy()[0]

def play_one_td(env, pmodel, vmodel, gamma = 0.95, render = False):
    total_reward = 0
    done = False
    obs = env.reset()
    iters = 0

    while not done:
        if render:
            env.render()
        action = pmodel.sample_action(obs)

        prev_obs = obs
        obs, reward, done, _ = env.step([action])
        total_reward += reward

        # update models
        V_next = vmodel.predict(obs)
        G = reward + gamma*V_next
        advantage = G - vmodel.predict(prev_obs)
        pmodel.partial_fit(prev_obs, action, advantage)
        vmodel.partial_fit(prev_obs, G)

        iters += 1
    return total_reward, iters

def main():
    env = gym.make("Pendulum-v0")
    ft = FeatureTransformer(env, n_components = 100)
    D = ft.dimensions
    pmodel = PolicyModel(ft, D, [])
    vmodel = ValueModel(ft, D, [])
    gamma = 0.95

    N = 50
    total_rewards = np.empty(N)
    costs = np.empty(N)
    for n in range(N):
        total_reward, num_steps = play_one_td(env, pmodel, vmodel, gamma)
        total_rewards[n] = total_reward
        if n%1 == 0:
            print("episode:", n, "total reward: %.1f" % total_reward, "num steps: %d" % num_steps, "avg reward (last 100): %.1f" % total_rewards[max(0, n-100):(n+1)].mean())
    print("avg reward for last 100 episodes:", total_rewards[-100:].mean())

    plt.plot(total_rewards)
    plt.title("Rewards")
    plt.xlabel("n")
    plt.show()

    plot_running_avg(total_rewards)
    # plot_cost_to_go(env, vmodel)

    tf.keras.utils.plot_model(pmodel.mean_model, CURRENT_DIR + "/pmodel_mean.png", show_shapes = True, expand_nested=False)
    tf.keras.utils.plot_model(pmodel.st_dev_model, CURRENT_DIR + "/pmodel_st_dev.png", show_shapes = True, expand_nested=False)
    tf.keras.utils.plot_model(vmodel.model, CURRENT_DIR + "/vmodel.png", show_shapes = True, expand_nested=False)

    env = gym.wrappers.Monitor(env, VIDEO_SAVE_PATH, force = True)
    play_one_td(env, pmodel, vmodel, gamma, True)

if __name__ == '__main__':
    main()