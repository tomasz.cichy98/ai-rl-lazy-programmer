# Deep Reinforcement Learning (PG)
## Policy Gradient Methods 
Now we want to parameterise the policy. The optimal policy is $`\pi^*`$.
First we define score for each action
```math
\textrm{score}_j = f(a_j,s,\theta) = (\phi (s)^T\theta_j\;\textrm{if linear})
```
Now we softmax to turn scores into probabilities that sum to 1
```math
\pi(a_j\;|\;s) = \frac{\exp(\textrm{score}_j)}{\sum_{j'}\exp(\textrm{score}_{j'})}
```
He have to define the objective for the policy and make it differentiable to use gradient descent/ascent. That is why the name is "policy gradient".  
Start in some state $`s_0`$. We want to maximize the expected return from this point
```math
\textrm{Performance}: \eta(\theta_p) = V_\pi(s_0)
```
Parameterise policy and value functions
```math
\textrm{Policy}: \pi(a\;|\;s,\;\theta_p) = f(s;\;\theta_p)\\
\textrm{Value function approx}: \hat{V}_\pi(s) = f(s;\;\theta_V)
```
Policy gradient theorem says that the gradient of the perofrmance is dependent on the gradient of the policy.
```math
\nabla\eta(\theta_p) = E\left[\sum_aQ_\pi(s,\;a)\nabla_{\theta_p}\pi(a\;|\;s,\;\theta_p)\right]
```
Multiply and divide by pi
```math
\nabla\eta (\theta_p) = E \left[\sum_a \pi(a|s,\theta_p) Q_\pi(s,a) \nabla_{\theta_p} \pi(a|s,\theta_p) \frac{1}{\pi(a|s,\theta_p)}\right]
```
It is now the expected value over pi
```math
\nabla\eta(\theta_p) = E \left\{ E \left[ Q_\pi(s,a) \nabla_{\theta_p} \pi(a|s,\theta_p) \frac{1}{\pi(a|s,\theta_p)} \right] \right\}
```
```math
\nabla\eta(\theta_p) = E \left[ Q_\pi(s,a) \nabla_{\theta_p} \pi(a|s,\theta_p) \frac{1}{\pi(a|s,\theta_p)} \right]
```
From calculus
```math
\nabla\log f(x) = \frac{\nabla f(x)}{f(x)}
```
so we get
```math
\nabla\eta(\theta_p) = E \left[ Q_\pi(s,a) \nabla_{\theta_p} \log \pi(a|s,\theta_p) \right]
```
Q is the expected value of G so we get
```math
\nabla\eta(\theta_p) = E \left[ G \nabla_{\theta_p} \log \pi(a|s,\theta_p) \right]
```
Now we can calculate G and pi.  
Use gradient ascent as we want to maximise the performance.  
We can do it in batches once the episode is over
```math
E(x) \approx \frac{1}{N}\sum_{n=1}^NX_n \\
\nabla\eta(\theta_p) \approx \frac{1}{T}\sum_{t=1}^T G_t \nabla_{\theta_p} \log \pi(a_t|s_t,\theta_p)
```
G is constant so we can put it inside the gradient
```math
\nabla\eta(\theta_p) \approx \frac{1}{T}\sum_{t=1}^T \nabla_{\theta_p} G_t \log \pi(a_t|s_t,\theta_p)
```
For the gradient 1/T is meaningless. We want to use TensorFlow's build in function so we have to change grad ascent to gradient descent. This is a MC method (it involves the sum over an episode)
```math
\textrm{max}:\sum_{t=1}^TG_t\log\pi(a_t|s_t,\theta_p)\\
\textrm{min}:-\sum_{t=1}^TG_t\log\pi(a_t|s_t,\theta_p)
```
Now the stochastic GD would look like that
```math
\theta_{P,t+1} = \theta_{P,t} + \alpha G_t \frac{\nabla \pi(a_t|s_t)}{\pi(a_t|s_t)} = \theta_{P,t} + \alpha G_t \nabla \log \pi(a_t|s_t)
```
The bigger the G the bigger the step we take in this direction. If pi is small but G is big we take an even bigger step in this direction.  
Common modification is to add V(s) as a baseline.
```math
\textrm{max}: \sum_{t=1}^T (G_t-V(s_t)) \log \pi(a_t|
s_t,\theta_p)
```
We call G - V(s) the advantage. It has been shown to have effect on variance of updates, speeding up learning.  
We update V as usual
```math
\theta_{V,t+1} = \theta_{V,t} + \alpha(G_t-V_t)\nabla V_t
```
This can be converted to a TD method. Is is called the "actor-critic" method where policy is the "actor" and TD error is the "critic".  
To do it replace G with the one-step estimate
```math
\theta_{P,t+1} = \theta_{P,t} + \alpha(r_{t+1} + \gamma V(s_{t+1}) - V(s_t))\nabla\log\pi(a_t|s_t) \\
\theta_{P,t+1} = \theta_{P,t} + \alpha(r_{t+1} + \gamma V(s_{t+1}) - V(s_t))\nabla V(s_t)
```

### Why use it?
- gives probabilistic policy
- more expresive than eps-greedy
- not all sub optimal actions have the same probability
- it may be optimal to do action 1:90%, 2:8%, 3:2%
- states may be stochastic

## Continuous Action Spaces
Use for example `MountainCarContinuous-v0`
The main idea so far:
- output policy model is a proability distribution on the action space
- we have used discrete distribution
- Now use a continuous distribution like Gaussian
Gaussian(Normal) has 2 parameters: mean and variance.  
Assuming linear parameterization of both
```math
\pi(a|s) = \frac{1}{\sqrt{2\pi v}} e^{-\frac{1}{2v}(a - \mu)^2} = N(a; \mu, v) \\
\mu(s;\theta_{\mu}) = \theta_{\mu}^T\phi(s) \\
v(s; \theta_v) = \exp(\theta_v^T\phi(s)) \\
\textrm{Alternative}: v(s;\theta_v) = softplus(\theta_v^T\phi(s)) \\
softplus(a) = \ln(1 + \exp(a))
```
Variance must be positive.  
Now the update of the model does not change at all. We can use grad descent or RMSprop or other
```math
\theta = \theta + \alpha\sum_{t=1}^T(G_t-V(s_t))\nabla\log\pi(a_t|s_t) \\ 
\theta_{t+1} = \theta_t +\alpha (r_{t+1} + \gamma V(s_{t+1}) - V(s_t))\nabla\log\pi(a_t|s_t)
```
With Q-Learning number of output nodes =  the numer of actions. It can not work for a continuous action spaces. Policy gradient makes it possible to solve such problems.

### MountainCarContinuous-v0
- Reach goal: +100
- Substract sum of squared actions
- Bigger action = bigger penalty
- Solved when reach a total reward of 90 avg over 100 episodes
- Action must be in [-1, +1]

We will not use gradient method. Use "hill climbing".  
1. Init parameters randomly
1. Play a few episodes with them
1. Slightly modify the "best so far" parameters and test them on a few episodes
1. If new parameters are better make them new "best so far"

This method works for finding good hyperparameters for DL models. It is not unieque to RL. 
