# MDPs
MDP is a collection of:
- all states
- all actions
- all rewards
- state transition probabilities p(s' , r | s, a)
- discount factor
    - return (reward with discount factor):
    ```math
    G(t) = \sum_{\tau = 0}^{\infty}\gamma^\tau R(t+\tau + 1)
    ```
Because rewards are probabilistic, the value function is also probabilistic.
The expected reward for being in the state $`s`$:  
```math
V_{\pi}(s) = E_{\pi}[G(t) \;|\; S_t = s] = E_{\pi}\left[\sum_{\tau = 0}^{\infty}\gamma^\tau R(t+\tau + 1) \;|\; S_t = s\right]
```

State-value function: $`V_\pi(s) = E_\pi\left[G(t)\;|\;S_t=s\right]`$  
Action-value function: $`Q(s,\; a) = E_\pi[G(t)\;|\;S_t = s,\; A_t = a]`$  
where $`\pi`$ is a policy.
$`Q`$ is better for control problems.  
How to find $`\pi`$:
- $` \pi(s) = \textrm{argmax}_a\{ \sum_{s'} p(s',\; r\;|\;s,\;a)(r+\gamma V(s')) \}`$
- $` \pi(s) = \textrm{argmax}_a\{ Q(s,\;a) \}`$
  
The value function is the expected future reward so the value of any terminal state is 0.

## Iterative policy evaluation
```math
V_\pi(s) = \sum_a\pi(a\;|\;s)\sum_{s'}\sum_r p(s',\; r\;|\; s,\; a) \{ r + \gamma V_k(s') \}
```
Policy iteration:
- while not converged:
    - policy evaluation on current policy
    - policy improvement (argmax over Q(s,\;a))

Value iteration:
```math
V_{k+1} = \textrm{max}_a\sum_{s'}\sum_rp(s',\;r \;|\; s,\; a)\{r + \gamma V_k(s') \}
```

## Monte Carlo
- play a bunch of episodes, then:
```math
V(s) = E \left[ G(t) \;|\; S(t) = s \right] = \frac{1}{N} \sum_{i=1}^{N} G_{i,\;s}
```
- init random policy
- while not converged
    - play episode, cumulate returns
    - do policy improvement based on current Q(s,a)

## Temporal Difference Learning
- current sample mean from the last sample mean:
```math
V_t(s) = V_{t-1} + \frac{1}{t}\left[ G_t - V_{t-1}(s) \right] = \frac{1}{t}\sum_{t'=1}^t G_{t'}
```
- it is gradient descent:
```math
V(s) = V(s) + \alpha[G - V(s)]
```

### TD(0)
- allows doing true online learning
- use r and V instead of G because episodes might not end
- update value of the state (time t) at t+1
```math
V(S_t) = V(S_t) + \alpha[r + \gamma V(S_{t+1}) - V(S_t)]
```

### SARSA
```math
Q(s,\; a) = Q(s,\; a) + \alpha[r + \gamma Q(s',\; a') - Q(s,\; a)] \qquad a' = \textrm{argmax}_a\{Q(s',\;a)\}
```
### Q-Learning
- similar to SARSA but "off policy"
- doesn't matter what action we take next, the target remains the same since it's a max
```math
Q(s,\; a) = Q(s,\; a) + \alpha[r + \gamma \textrm{max}_{a'}Q(s',\;a') - Q(s,\;a)]
```

## Approximation Methods
- we want to estimate V or Q  
    INPUT: $`x = \phi(s)`$, TARGET: G
- G is a real number so we do regression and the appropriate loss is squared error
```math
E = \sum_{n=1}^{N}(G_n - \hat{V}(\phi(S_n);\; \theta ))^2
```
- we need a differentiable model so we can do gradient descent
```math
\hat{V}(\phi{S_n};\; \theta) = \theta^T\phi(S_n)
```
- Theta is the model parameter so we update:
```math
\theta = \theta - \alpha\frac{\partial E}{\partial \theta} = \theta + \alpha(G - \hat{V})\phi(s)
```
