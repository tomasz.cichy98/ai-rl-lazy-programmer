from worker import Worker
from nets import create_networks
import gym
import sys
import os
import numpy as np
# import tensorflow as tf
import matplotlib.pyplot as plt
import itertools
import shutil
import threading
import multiprocessing
import time

# use with tf1 virtual env
import tensorflow as tf
# import tensorflow.compat.v1 as tf
# tf.disable_v2_behavior())

# ENV_NAME = "Breakout-v0"
ENV_NAME = "BeamRider-v0"
# ENV_NAME = "Pong-v0"
# ENV_NAME = "BattleZone-v0"
MAX_GLOBAL_STEPS = 1e6
MAX_TIME = 8.5*60*60 # 8.5 hours
# MAX_TIME = 15*60 # for tests
STEPS_PER_UPDATE = 5

print("==============")
print(tf.__version__)
print("==============")


def Env():
    return gym.envs.make(ENV_NAME)


# Depending on the game we may have a limited action space
if ENV_NAME == "Pong-v0" or ENV_NAME == "Breakout-v0":
    NUM_ACTIONS = 4  # env.action_space.n returns a bigger number
else:
    env = Env()
    NUM_ACTIONS = env.action_space.n
    env.close()


def smooth(x):
    n = len(x)
    y = np.zeros(n)
    for i in range(n):
        start = max(0, i - 99)
        y[i] = float(x[start:(i+1)].sum())/(i-start+1)
    return y

# what does the model see
def show_image_transformer(sess):
    env = Env()
    im = env.reset()

    # collect images
    im_arr = []
    im_arr.append(im)

    # transform
    image_transformer = ImageTransformer()
    im = image_transformer.transform(im, sess)

    im_arr.append(im)

    # display
    for im in im_arr:
        plt.figure()
        plt.imshow(im)


NUM_WORKERS = multiprocessing.cpu_count()
START_TIME = time.time()

with tf.device("/cpu:0"):
    # keep track of number of updates
    global_step = tf.Variable(0, name="global_step", trainable=False)

    # global policy
    with tf.variable_scope("global") as vs:
        policy_net, value_net = create_networks(NUM_ACTIONS)

    # global iterator
    global_counter = itertools.count()

    # save returns
    # list is passed as a reference so the same list is updated by all workers
    returns_list = []

    # create workers
    workers = []
    for worker_id in range(NUM_WORKERS):
        worker = Worker(name=f"worker_{worker_id}", env=Env(), policy_net=policy_net, value_net=value_net,
                        global_counter=global_counter, returns_list=returns_list, max_time = MAX_TIME, discount_factor=0.99, max_global_steps=MAX_GLOBAL_STEPS, start_time = START_TIME)
        workers.append(worker)

with tf.Session() as sess:
#     show_image_transformer(sess)
    sess.run(tf.global_variables_initializer())
    coord = tf.train.Coordinator()

    # start workers
    worker_threads = []
    for worker in workers:
        worker_fn = lambda:worker.run(sess, coord, STEPS_PER_UPDATE)
        t = threading.Thread(target=worker_fn)
        t.start()
        worker_threads.append(t)

    # wait for the threads to end
    coord.join(worker_threads, stop_grace_period_secs = 300)

    # plot
    x = np.array(returns_list)
    x_smooth = smooth(x)
    plt.plot(x, label = "orig")
    plt.plot(x_smooth, label = "smoothed")
    plt.title("Reward over time")
    plt.xlabel("episode")
    plt.ylabel("reward")
    plt.legend()
    plt.show()

    # render a game
    for worker in workers:
        worker_fn = lambda:worker.run_render(sess)
        t = threading.Thread(target=worker_fn)
        t.start()
        worker_threads.append(t)

    # wait for the threads to end
    coord.join(worker_threads, stop_grace_period_secs = 300)