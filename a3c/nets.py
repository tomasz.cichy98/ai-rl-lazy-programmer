# use with tf1 virtual env
import tensorflow as tf
# import tensorflow.compat.v1 as tf
# tf.disable_v2_behavior()

def build_feature_extractor(input_):
    # create weights once

    input_ = tf.to_float(input_)/255.0

    # conv layers
    conv1 = tf.contrib.layers.conv2d(input_,
                                     16,  # output feature maps
                                     4,  # kernel size
                                     2,  # stride
                                     activation_fn=tf.nn.relu,
                                     scope="conv1"
                                     )

    conv2 = tf.contrib.layers.conv2d(input_,
                                     32,  # output feature maps
                                     4,  # kernel size
                                     2,  # stride
                                     activation_fn=tf.nn.relu,
                                     scope="conv2"
                                     )

    # image -> feature vector
    flat = tf.contrib.layers.flatten(conv2)

    # dense
    fc1 = tf.contrib.layers.fully_connected(
        inputs=flat,
        num_outputs=256,
        scope="fc1"
    )

    return fc1


class PolicyNetwork:
    def __init__(self, num_outputs, reg=0.01):
        self.num_outputs = num_outputs

        # graph inputs
        # use 4 consecutive frames
        self.states = tf.placeholder(
            shape=[None, 84, 84, 4], dtype=tf.uint8, name="X")
        # Adv = G - V(s)
        self.advantage = tf.placeholder(shape=[None], dtype=tf.float32, name="y")
        # selected actions
        self.actions = tf.placeholder(shape=[None], dtype=tf.int32, name="actions")

        # reuse = False so we must create Policy before the Value network
        # ValueNet will have reuse = True
        with tf.variable_scope("shared", reuse=False):
            fc1 = build_feature_extractor(self.states)

        # separate scope for output and loss
        with tf.variable_scope("policy_network"):
            self.logits = tf.contrib.layers.fully_connected(
                fc1, num_outputs, activation_fn=None)
            self.probs = tf.nn.softmax(self.logits)

            # sample action
            cdist = tf.distributions.Categorical(logits=self.logits)
            self.sample_action = cdist.sample()

            # add regularization
            self.entropy = -tf.reduce_sum(self.probs * tf.log(self.probs), axis=1)

            # get preds for the chosen action
            batch_size = tf.shape(self.states)[0]
            gather_indices = tf.range(batch_size) * tf.shape(self.probs)[1] + self.actions
            self.selected_action_probs = tf.gather(
                tf.reshape(self.probs, [-1]), gather_indices)

            self.loss = tf.log(self.selected_action_probs) * self.advantage + reg * self.entropy
            self.loss = -tf.reduce_sum(self.loss, name="loss")

            # training
            self.optimizer = tf.train.RMSPropOptimizer(0.00025, 0.99, 0.0, 1e-6)

            # needed later
            self.grads_and_vars = self.optimizer.compute_gradients(self.loss)
            self.grads_and_vars = [[grad, var] for grad, var in self.grads_and_vars if grad is not None]


class ValueNetwork:
    def __init__(self):
        # input placeholders
        self.states = tf.placeholder(
            shape=[None, 84, 84, 4], dtype=tf.uint8, name="X")
        # TD target
        self.targets = tf.placeholder(shape=[None], dtype=tf.float32, name="y")

        with tf.variable_scope("shared", reuse=True):
            fc1 = build_feature_extractor(self.states)

        with tf.variable_scope("value_network"):
            self.vhat = tf.contrib.layers.fully_connected(
                inputs=fc1, num_outputs=1, activation_fn=None)
            self.vhat = tf.squeeze(self.vhat, squeeze_dims=[1], name="vhat")

            self.loss = tf.squared_difference(self.vhat, self.targets)
            self.loss = tf.reduce_sum(self.loss, name="loss")

            # training
            self.optimizer = tf.train.RMSPropOptimizer(
                0.00025, 0.99, 0.0, 1e-6)

            # needed later for grad desc
            self.grads_and_vars = self.optimizer.compute_gradients(self.loss)
            self.grads_and_vars = [[grad, var] for grad, var in self.grads_and_vars if grad is not None]

# create nets in the correct order
def create_networks(num_outputs):
    policy_network = PolicyNetwork(num_outputs = num_outputs)
    value_network = ValueNetwork()
    return policy_network, value_network