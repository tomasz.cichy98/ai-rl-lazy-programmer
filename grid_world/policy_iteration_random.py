import numpy as np
from grid_world import standard_grid, negative_grid
from iterative_policy_evaluation import print_values, print_policy

CONVERGENCE_TRESHOLD = 10e-3
GAMMA = 0.9
ALL_ACTIONS = ("U", 'D', 'L', 'R')

# Most of the code here is copied and pasted form policy_iteration.py

if __name__ == "__main__":
    grid = negative_grid()

    # print revards for each grid cell
    print("rewards:")
    print_values(grid.rewards, grid)

    # int policy
    policy = {}
    for s in grid.actions.keys():                   # for each possible state
        policy[s] = np.random.choice(ALL_ACTIONS)   # assign random action as policy

    # print policy
    print("initial policy:")
    print_policy(policy, grid)

    # int value function
    V = {}
    states = grid.all_states()                      # all possible states
    for s in states:
        if s in grid.actions:                       # if there is an action for this state
            V[s] = np.random.random()               # assign the value of the action to a random number
        else:                                       # terminal state
            V[s] = 0                                # value of the terminal state is zero

    while True:                                     # break when policy does not change
        while True:                                 # policy evaluation
            biggest_change = 0                      # all the same stuff as in iterative_policy_evaluation.py
            for s in states:
                old_v = V[s]

                # init new_v
                new_v = 0
                if s in policy:                     # if there is a policy for s
                    for a in ALL_ACTIONS:           # for all possible actions
                        if a == policy[s]:          # there is 50% chance of following the policy
                            p = 0.5
                        else:                       # there is a 50 % chance of doing any other action
                            p = 0.5/3
                        grid.set_state(s)           # do the action
                        r = grid.move(a)            # get the reward
                        new_v += p*(r+GAMMA*V[grid.current_state()])
                    V[s] = new_v
                    biggest_change = max(biggest_change, np.abs(old_v - V[s]))

            if biggest_change < CONVERGENCE_TRESHOLD:
                break
            
        # policy improvement
        is_policy_converged = True
        for s in states:                        # for all possible states
            if s in policy:                     # if there is a policy for this state
                old_a = policy[s]               # old action = current policy[s]
                new_a = None                    # no new action
                best_value = float('-inf')      # best value of an action is -inf

                for a in ALL_ACTIONS:           # for all possible actions
                    v = 0
                    for a2 in ALL_ACTIONS:
                        if a == a2:
                            p = 0.5
                        else:
                            p = 0.5/3
                        grid.set_state(s)       # do the action
                        r = grid.move(a)        # get the reward
                        v += p*(r + GAMMA * V[grid.current_state()]) # evaluate the action
                    if v > best_value:          # if the value of this action if better than a current best action
                        best_value = v          # make it's value new best value
                        new_a = a               # make this action new best action
                policy[s] = new_a               # make best action be a new policy
                if new_a != old_a:              # was there a change to the policy?
                    is_policy_converged = False

        if is_policy_converged:
            break


    print("Optimal value function:")
    print_values(V, grid)
    print("Optimal policy:")
    print_policy(policy,grid)

