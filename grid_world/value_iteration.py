import numpy as np
from grid_world import standard_grid, negative_grid
from iterative_policy_evaluation import print_values, print_policy

CONVERGENCE_TRESHOLD = 10e-3
GAMMA = 0.9
ALL_ACTIONS = ("U", 'D', 'L', 'R')

# most of the code is from policy_iteration.py

if __name__ == "__main__":
    grid = negative_grid()

    # print revards for each grid cell
    print("rewards:")
    print_values(grid.rewards, grid)

    # int policy
    policy = {}
    for s in grid.actions.keys():                   # for each possible state
        policy[s] = np.random.choice(ALL_ACTIONS)   # assign random action as policy

    # print policy
    print("initial policy:")
    print_policy(policy, grid)

    # int value function
    V = {}
    states = grid.all_states()                      # all possible states
    for s in states:
        if s in grid.actions:                       # if there is an action for this state
            V[s] = np.random.random()               # assign the value of the action to a random number
        else:                                       # terminal state
            V[s] = 0                                # value of the terminal state is zero

    while True:                                     # break when policy does not change
        biggest_change = 0                          
        for s in states:
            old_v = V[s]

            if s in policy:
                new_v = float("-inf")               # smallest number
                for a in ALL_ACTIONS:               # for all possible actions
                    grid.set_state(s)               # do the action
                    r = grid.move(a)                # get the reward
                    v = r + GAMMA * V[grid.current_state()] # eval the action
                    if v > new_v:                   # max V for the state
                        new_v = v
                V[s] = new_v
                biggest_change = max(biggest_change, np.abs(old_v - V[s]))
        if biggest_change < CONVERGENCE_TRESHOLD:
            break
        
        # policy improvement
        for s in policy.keys():
            best_a = None                           # best action
            best_value = float("-inf")              # best value (of action)

            for a in ALL_ACTIONS:                   # for all possible actions
                grid.set_state(s)                   # do the action
                r = grid.move(a)                    # get the reward
                v = r + GAMMA * V[grid.current_state()] # eval action
                if v > best_value:                  # find best action for a state
                    best_value = v
                    best_a = a
            policy[s] = best_a                      # update the policy


    print("Optimal value function:")
    print_values(V, grid)
    print("Optimal policy:")
    print_policy(policy,grid)

