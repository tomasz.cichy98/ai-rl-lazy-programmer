import numpy as np

class Grid:
    def __init__(self, rows, cols, start):
        self.rows = rows
        self.cols = cols
        self.i = start[0]
        self.j = start[1]
    
    def set(self, rewards, actions):
        # rewards should be a dict of: (i, j): r (row, col): reward
        # actions should be a dict of: (i, j): A (row, col): list of possible actions
        self.rewards = rewards
        self.actions = actions

    def set_state(self, s):
        self.i, self.j = s[0], s[1]
    
    def current_state(self):
        return (self.i, self.j)

    def is_terminal(self,s):
        return s not in self.actions

    def move(self, action):
        # check if legal move
        if action in self.actions[(self.i, self.j)]:
            if action == 'U':
                self.i -= 1
            elif action == 'D':
                self.i += 1
            elif action == 'R':
                self.j += 1
            elif action == 'L':
                self.j -= 1

        # return reward
        return self.rewards.get((self.i, self.j), 0)

    def undo_move(self, action):
        if action in self.actions[(self.i, self.j)]:
            if action == 'U':
                self.i += 1
            elif action == 'D':
                self.i -= 1
            elif action == 'R':
                self.j -= 1
            elif action == 'L':
                self.j += 1

        # whe should never get here
        assert(self.current_state() in self.all_states())

    def game_over(self):
        # true if no actions are possible
        return (self.i, self.j) not in self.actions

    def all_states(self):
        return set(self.actions.keys()) | set(self.rewards.keys())

def standard_grid():
    """
    define a grid that describes the reward for arriving at each state
    and possible actions at each state
    the grid looks like this
    x means you can't go there
    s means start position
    number means reward at that state
    .  .  .  1
    .  x  . -1
    s  .  .  .
    """

    g = Grid(3, 4, (2, 0))
    rewards = {(0, 3) : 1, (1, 3) : -1}
    actions = {
        (0, 0) : ('D', 'R'),
        (0, 1) : ('L', 'R'),
        (0, 2) : ('L', 'D', 'R'),
        (1, 0) : ('U', 'D'),
        (1, 2) : ('U', 'D', 'R'),
        (2, 0) : ('U', 'R'),
        (2, 1) : ('L', 'R'),
        (2, 2) : ('L', 'R', 'U'),
        (2, 3) : ('L', 'U')
    }

    g.set(rewards, actions)

    return g

def negative_grid(step_cost = -0.1):
    """
    We want to minimize the number of moves
    so penalize ever move
    """

    g = standard_grid()
    g.rewards.update({
        (0, 0) : step_cost,
        (0, 1) : step_cost,
        (0, 2) : step_cost,
        (1, 0) : step_cost,
        (1, 2) : step_cost,
        (2, 0) : step_cost,
        (2, 1) : step_cost,
        (2, 2) : step_cost,
        (2, 3) : step_cost
    })

    return g

def big_grid():
    g = Grid(7, 9, (6, 0))
    rewards = {
        (0, 8) : 1,
        (1, 3) : -1,
        (2, 3) : -1,
        (3, 3) : -1,
        (1, 2) : -1,
        (2, 2) : -1,
        (3, 2) : -1,
        (1, 1) : -1,
        (2, 1) : -1,
        (3, 1) : -1,
        (6, 4) : -1,
        (6, 5) : -1,
        (1, 8) : -1,
        (0, 0) : -1
    }

    unreachable = {
        (2, 5),
        (5, 1),
        (0, 6)
    }

    actions = generate_possible_action_for_a_grid(g, rewards, unreachable)

    g.set(rewards, actions)

    return g

def negative_big_grid(step_cost = -0.005):
    g = big_grid()

    updates = {}
    for key in g.actions.keys():
        updates[key] = step_cost

    g.rewards.update(updates)

    return g

def generate_possible_action_for_a_grid(g, rewards, unreachable):
    actions = {}
    for i in range(g.rows):
        for j in range(g.cols):
            actions_list = []

            if (i, j) in unreachable:
                continue

            if (i, j) in rewards:
                continue

            # check up
            if i > 0 and (i - 1, j) not in unreachable:
                actions_list.append("U")
            # check down
            if i < g.rows - 1 and (i + 1, j) not in unreachable:
                actions_list.append("D")
            # check left
            if j > 0 and (i, j - 1) not in unreachable:
                actions_list.append("L")
            # check right
            if j < g.cols - 1 and (i, j + 1) not in unreachable:
                actions_list.append("R")

            # convert to actions
            actions[(i,j)] = tuple(actions_list)

    return actions
