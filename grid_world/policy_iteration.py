import numpy as np
from grid_world import standard_grid, negative_grid
from iterative_policy_evaluation import print_values, print_policy

CONVERGENCE_TRESHOLD = 10e-3
GAMMA = 0.9
ALL_ACTIONS = ("U", 'D', 'L', 'R')

if __name__ == "__main__":
    grid = negative_grid()

    # print revards for each grid cell
    print("rewards:")
    print_values(grid.rewards, grid)

    # int policy
    policy = {}
    for s in grid.actions.keys():                   # for each possible state
        policy[s] = np.random.choice(ALL_ACTIONS)   # assign random action as policy

    # print policy
    print("initial policy:")
    print_policy(policy, grid)

    # int value function
    V = {}
    states = grid.all_states()                      # all possible states
    for s in states:
        if s in grid.actions:                       # if there is an action for this state
            V[s] = np.random.random()               # assign the value of the action to a random number
        else:                                       # terminal state
            V[s] = 0                                # value of the terminal state is zero

    while True:                                     # break when policy does not change
        while True:                                 # policy evaluation
            biggest_change = 0                      # all the same stuff as in iterative_policy_evaluation.py
            for s in states:
                old_v = V[s]

                if s in policy:
                    a = policy[s]
                    grid.set_state(s)           # do the action
                    r = grid.move(a)            # get the reward
                    V[s] = r + GAMMA * V[grid.current_state()]
                    biggest_change = max(biggest_change, np.abs(old_v - V[s]))
            if biggest_change < CONVERGENCE_TRESHOLD:
                break
            
        # policy improvement
        is_policy_converged = True
        for s in states:                        # for all possible states
            if s in policy:                     # if there is a policy for this state
                old_a = policy[s]               # old action = current policy[s]
                new_a = None                    # no new action
                best_value = float('-inf')      # best value of an action is -inf

                for a in ALL_ACTIONS:           # for all possible actions
                    grid.set_state(s)           # do the action
                    r = grid.move(a)            # get the reward
                    v = r + GAMMA * V[grid.current_state()] # evaluate the action
                    if v > best_value:          # if the value of this action if better than a current best action
                        best_value = v          # make it's value new best value
                        new_a = a               # make this action new best action
                policy[s] = new_a               # make best action be a new policy
                if new_a != old_a:              # was there a change to the policy?
                    is_policy_converged = False

        if is_policy_converged:
            break


    print("Optimal value function:")
    print_values(V, grid)
    print("Optimal policy:")
    print_policy(policy,grid)

