import numpy as np
import matplotlib.pyplot as plt
from grid_world import standard_grid, negative_grid
from iterative_policy_evaluation import print_values, print_policy
from monte_carlo_es import max_dict

CONVERGENCE_TRESHOLD = 10e-3
GAMMA = 0.9
ALL_ACTIONS = ("U", 'D', 'L', 'R')

# NOTE: find optimal policy and value function
#       using on-policy first-visit MC

def random_action(a, eps = 0.1):
    #  choose given a with probability 1 - eps + eps/4
    # choose some other a' != a with probability eps/4
    p = np.random.random()
    if p < (1-eps):
        return a
    else:
        return np.random.choice(ALL_ACTIONS)

def play_game(grid, policy):
    # returns a list of states and corresponding returns
    # in this version we will NOT use "exploring starts" method
    # instead we will explore using an epsilon-soft policy
    s = (2, 0)
    grid.set_state(s)
    a = random_action(policy[s])

    states_actions_rewards = [(s,a,0)]
    while True:
        r = grid.move(a)
        s = grid.current_state()
        if grid.game_over():
            states_actions_rewards.append((s, None, r))
            break
        else:
            a = random_action(policy[s])
            states_actions_rewards.append((s,a,r))

    # calculate the returns by working backwards from the terminal state
    G = 0
    states_actions_returns = []
    first = True
    for s,a,r in reversed(states_actions_rewards):
        # the value of the terminal state is 0 by definition
        # we should ignore the first state we encounter
        # and ignore the last G, which is meaningless since it doesn't correspond to any move
        if first:
            first = False
        else:
            states_actions_returns.append((s,a,G))
        G = r+GAMMA*G
    states_actions_returns.reverse()
    return states_actions_returns

if __name__ == "__main__":
    # use the standard grid again (0 for every step) so that we can compare
    # to iterative policy evaluation
    # grid = standard_grid()
    # try the negative grid too, to see if agent will learn to go past the "bad spot"
    # in order to minimize number of steps 
    grid = negative_grid()

    # print rewards
    print("rewards:")
    print_values(grid.rewards, grid)

    # state -> action
    # init random policy
    policy = {}
    for s in grid.actions.keys():
        policy[s] = np.random.choice(ALL_ACTIONS)

    # init Q(s,a)
    Q = {}
    returns = {}
    states = grid.all_states()
    for s in states:
        if s in grid.actions:           # not terminal
            Q[s] = {}
            for a in ALL_ACTIONS:
                Q[s][a] = 0             # init 
                returns[(s, a)] = []
        else:                           # terminal state
            pass

    # until covenrgence
    deltas = []
    for t in range(5000):
        if t % 1000 == 0:
            print(t)

        # generate episode with t
        biggest_change = 0
        states_actions_returns = play_game(grid, policy)

        seen_states_action_pairs = set()
        for s, a, G in states_actions_returns:
            # first visit MC
            sa = (s,a)
            if sa not in seen_states_action_pairs:
                old_q = Q[s][a]
                returns[sa].append(G)
                Q[s][a] = np.mean(returns[sa])
                biggest_change = max(biggest_change, np.abs(old_q - Q[s][a]))
                seen_states_action_pairs.add(a)
        deltas.append(biggest_change)

        # update policy
        for s in policy.keys():
            a, _ = max_dict(Q[s])
            policy[s] = a


    plt.plot(deltas)
    plt.xlabel("t")
    plt.ylabel('delta')
    plt.title('Change in Q over time')
    plt.show()

    # find V
    V = {}
    for s in policy.keys():
        V[s] = max_dict(Q[s])[1]

    print("final vals:")
    print_values(V, grid)

    print("final policy:")
    print_policy(policy, grid)
