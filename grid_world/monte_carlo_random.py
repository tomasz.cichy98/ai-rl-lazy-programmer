import numpy as np
from grid_world import standard_grid, negative_grid
from iterative_policy_evaluation import print_values, print_policy

CONVERGENCE_TRESHOLD = 10e-3
GAMMA = 0.9
ALL_ACTIONS = ("U", 'D', 'L', 'R')

def random_action(a):
    # choose a given action with 0.5 prob
    # choose any other a with prob 0.5/3
    p = np.random.random()
    if p < 0.5:
        return a
    else:
        tmp = list(ALL_ACTIONS)
        tmp.remove(a)
        return np.random.choice(tmp)

def play_game(grid, policy):
    """
    Almost the same as in monte_carlo.py
    Return list of states and corresponding values
    Reset game to start at random pos
    This is needed because otherwise we would never end up at certain states
    """

    start_states = list(grid.actions.keys())
    start_idx = np.random.choice(len(start_states))
    grid.set_state(start_states[start_idx])

    s = grid.current_state()
    states_and_rewards = [(s, 0)]   # no reward for just starting a game
    while not grid.game_over():
        a = policy[s]
        a = random_action(a)
        r = grid.move(a)
        s = grid.current_state()
        states_and_rewards.append((s,r))

    # calc return working backwords
    G = 0
    states_and_returns = []
    first = True
    for s, r in reversed(states_and_rewards):
        # value of terminal state is 0
        # ignore first state
        # ignore last G as it doesn't correspond to any move
        if first:
            first = False
        else:
            states_and_returns.append((s, G))
        G = r + GAMMA*G
    states_and_returns.reverse()
    return states_and_returns

if __name__ == "__main__":
    grid = standard_grid()
    # grid = negative_grid()

    # print rewards
    print("rewards: ")
    print_values(grid.rewards, grid)

    # state -> action
    policy = {
        (2, 0): 'U',
        (1, 0): 'U',
        (0, 0): 'R',
        (0, 1): 'R',
        (0, 2): 'R',
        (1, 2): 'R',
        (2, 1): 'R',
        (2, 2): 'R',
        (2, 3): 'U'
    }

    # init V(s) and returns
    V = {}
    returns = {}
    states = grid.all_states()
    for s in states:            # for each state
        if s in grid.actions:   # if there is a action for this state
            returns[s] = []     # the return of this state is "empty"
        else:
            # terminal or impossible state
            V[s] = 0

    # repeat
    for t in range(5000):
        # generate episode using pi
        states_and_returns = play_game(grid,policy)
        seen_states = set()
        for s, G in states_and_returns:
            if s not in seen_states:        # if we have not visited s before
                # first-visit MC policy evaluation
                returns[s].append(G)
                V[s] = np.mean(returns[s])
                seen_states.add(s)

    
    print("values:")
    print_values(V, grid)
    print("policy:")
    print_policy(policy, grid)
