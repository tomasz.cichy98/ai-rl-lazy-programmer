import numpy as np
from grid_world import standard_grid

CONVERGENCE_TRESHOLD = 1e-3

def print_values(V, g):
    """
    Loop through the grid and print values associated with each cell
    """
    print("Printing values:")
    for i in range(g.rows):
        print("-------"*g.cols)
        for j in range(g.cols):
            v = V.get((i, j), 0)
            if v >= 0:
                print(" %.3f|"%v, end = "")
            else:
                print("%.3f|"%v, end = "") # extra -  space
        print("")
    print("\n")

def print_policy(P, g):
    """
    Loop through the grid and print actions associated with each cell
    """
    print("Printing policy:")
    for i in range(g.rows):
        print("----"*g.cols)
        for j in range(g.cols):
            a = P.get((i, j), ' ')
            print(" %s |"%a, end = "")
        print("")
    print("\n")


if __name__ == "__main__":
    """
    Iterative Policy Evaluation
    given a policy, find its value function V(s)
    do this for uniform random, and fixed policy

    2 sources of randomness
    p(a|s) - deciding what action to take
    p(s',r|s,a) - the next state and reward given action-state
    In this case p(a|s) = uniform    
    """
    grid = standard_grid()

    states = grid.all_states()

    # uniform random actions
    V = {}              # the value function
    for s in states:
        V[s] = 0        # V(s) = 0
    gamma = 1.0         # discount factor

    while True:
        biggest_change = 0
        for s in states:                        # for every possible state
            old_v = V[s]

            if s in grid.actions:               # if there is an action for this state
                new_v = 0                       # value of this action is 0
                p_a = 1.0/len(grid.actions[s])  # probability of this action is... equal probabilities
                for a in grid.actions[s]:       # for every possible action for this grid position
                    grid.set_state(s)           # do the action
                    r = grid.move(a)            # get the reward
                    new_v += p_a * (r + gamma * V[grid.current_state()])    # evaluate the action
                V[s] = new_v                    # update the value function
                biggest_change = max(biggest_change,np.abs(old_v - V[s]))   # find the biggest change(convergenceTreshold)
        if biggest_change < CONVERGENCE_TRESHOLD:                           # if the changes are big reevaluate the Value function
            break
    print("values for uniform random actions:")
    print_values(V, grid)
    print("\n\n")

    # Fixed policy
    # the the agent what to do for ever grid position
    policy = {
        (2, 0): 'U',
        (1, 0): 'U',
        (0, 0): 'R',
        (0, 1): 'R',
        (0, 2): 'R',
        (1, 2): 'R',
        (2, 1): 'R',
        (2, 2): 'R',
        (2, 3): 'U',
    }
    print("Fixed policy:")
    print_policy(policy, grid)
    
    V = {}
    for s in states:
        V[s] = 0
    
    gamma = 0.9

    while True:
        biggest_change = 0
        for s in states:
            old_v = V[s]

            if s in policy:         # for every possible state
                a = policy[s]       # take the action from the policy
                grid.set_state(s)   # do the action
                r = grid.move(a)    # get the reward
                V[s] = r + gamma * V[grid.current_state()]  #evaluate the action
                biggest_change = max(biggest_change, np.abs(old_v - V[s]))

        if biggest_change < CONVERGENCE_TRESHOLD:
            break

    print("values for fixed policy")
    print_values(V, grid)