from grid_world import standard_grid, negative_grid, big_grid, negative_big_grid
from iterative_policy_evaluation import print_values, print_policy

def print_grid(g):
    """
    Loop through the grid and print actions associated with each cell
    """
    print("Printing grid:")
    print("X: not accessible, 1: win, -1: lose")
    for i in range(g.rows):
        print("----"*g.cols)
        for j in range(g.cols):
            # start
            if i == g.i and j == g.j:
                print(" S |", end = "")
                continue

            if (i, j) in g.actions:
                # is a path
                # print empty
                print("   |", end = "")
            
            # is final spot
            if (i, j) in g.rewards:
                if g.rewards[(i, j)] == 1:
                    print(" 1 |", end = "")
                elif g.rewards[(i, j)] == -1:
                    print(" -1|", end = "")

            # is not accessible
            if (i, j) not in g.rewards and (i, j) not in g.actions:
                print(" X |", end = "")

        print("")
    print("\n")

# grid = big_grid()
grid = negative_big_grid()
# grid = standard_grid()
# grid = negative_grid()

print_values(grid.rewards, grid)
print_grid(grid)