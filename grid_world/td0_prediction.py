import numpy as np
import matplotlib as plt
from grid_world import standard_grid, negative_grid
from iterative_policy_evaluation import print_values, print_policy

CONVERGENCE_TRESHOLD = 10e-3
GAMMA = 0.9
ALPHA = 0.1
ALL_ACTIONS = ("U", 'D', 'L', 'R')

# only evaluation, not optimization

def random_action(a, eps = 0.1):
    # use epsilon soft to ensure all states are visited
    p = np.random.random()
    if p < (1 - eps):
        return a
    else:
        return np.random.choice(ALL_ACTIONS)

def play_game(grid, policy):
    # return list of states-rewards 
    # start at the given state
    s = (2, 0)
    grid.set_state(s)
    states_and_rewards = [(s, 0)]
    while not grid.game_over():
        a = policy[s]
        a = random_action(a)
        r = grid.move(a)
        s = grid.current_state()
        states_and_rewards.append((s, r))
    return states_and_rewards

if __name__ == "__main__":
    # use standard grid
    grid = standard_grid()

    print("rewards:")
    print_values(grid.rewards, grid)

    # state -> action
    policy = {
        (2, 0): 'U',
        (1, 0): 'U',
        (0, 0): 'R',
        (0, 1): 'R',
        (0, 2): 'R',
        (1, 2): 'R',
        (2, 1): 'R',
        (2, 2): 'R',
        (2, 3): 'U',
    }

    # init V(s) and returns
    V = {}
    states = grid.all_states()
    for s in states:
        V[s] = 0

    for it in range(1000):
        # use pi
        states_and_rewards = play_game(grid, policy)
        # first and last state are not important
        for t in range(len(states_and_rewards) - 1):
            s, _ = states_and_rewards[t]
            s2, r = states_and_rewards[t + 1]
            # update
            V[s] = V[s] + ALPHA*(r + GAMMA*V[s2] - V[s])

    print("values:")
    print_values(V, grid)
    print("policy:")
    print_policy(policy, grid)