# Deep Reinforcement Learning
## Radial Basis Function Networks
RBF is a linear model with feature extraction (RBF kernel). It is also a 1-hidden layer NN with RBF kernel as activation.  
```math
\phi(x) = \exp \left(-\frac{|x-c|^2}{\sigma^2} \right)
```
- x = input vector
- c = center
- Max is 1, when x == c, approaches 0 as x goes further away from c.

### Implementation
```python
from sklearn.kernel_approximation import RBFSampler

sampler = RBFSampler()
sampler.fit(raw_data)
features = sampler.transform(raw_data)
```

We can use multiple RBF samplers simultaneously with different parameters. We concatenate any features, not just those from RBFSampler.

```python
from sklearn.pipeline import FeatureUnion
```

Standardise data:

```python
from sklearn.preprocessing import StandardScaler
```
SGDRegressor requires running `partial_fit()` (one step of gradient descent) at least once to init variables. We run it with dummy variables.

```python
input = transform(env.reset()), target = 0
model.partial_fit(input, target)
```
After calling `partial_fit()` with target 0 we will get all predictions = 0 for a while.  
In Mountain Car all rewards are = -1, therefore, a Q prediction of 0 is higher than anything we can get. This is the **optimistic initial values** method.  

Instead of x <- transform(s, a) we do x <- transform(s). Now we have different Q(s) for every a. Mountain Car has 3 actions: left, right, nothing. It is now a neural network with 3 output nodes.

### Cost-To-Go function
Is the negative of the optimal value function V*(s). 2 state variables give a 3D plot.

## N-Step Methods & TD($`\lambda`$)
### N-Step
$`\lambda = 0`$ gives TD(0), $`\lambda = 1`$ gives Monte Carlo.  
Value function recursively (Bellman's equation):
```math
V(s) = \sum_a \pi(a \; | \; s)\sum_{s',\;r}p(s', r | s, a)\{r + \gamma V(s')\}
```
Estimate V by taking the sample mean of G's:
```math
V(s) \approx \frac{1}{N}\sum_{i=1}^{N}G_{i, s}
```
TD(0) combines these 2 things to estimate G:
```math
G_s \approx r + \gamma V(s')
```
And now we get (Wait N steps to update V):
```math
G^{(n)}(t) = R(t+1) + \gamma R(t + 2) + \dots + \gamma ^{n-1}R(t+n) + \gamma^n V(s(t+n))
```
For the actual update:
```math
\textrm{Tabular}: V(s(t)) = V(s(t)) + \alpha(G^{(n)}(t) - V(s(t)))
```
```math
\textrm{Function Approx}: \theta = \theta + \alpha(G^{(n)}(t) - V(s(t)))\frac{\partial V(s(t))}{\partial \theta}
```

### TD($`\lambda`$)
We can update after one step.  
We can combine returns by multiplying them by $`\lambda_n`$ as long as all lambdas add up to 1.
```math
\lambda_1G^{(1)}(t) + \lambda_2G^{(2)}(t) + \dots + \lambda_nG^{(n)}(t), \qquad \sum_{i=i}^n\lambda_i = 1
```
In TD($`\lambda`$) we make the coefficients decrease geometrically. $` \lambda \in [0,\; 1]`$
```math
\lambda^0G^{(1)}(t) + \lambda^1G^{(2)}(t) + \dots + \lambda^{n-1}G^{(n)}(t)
```
but now lambdas do not add up to 0.  
We know that
```math
\sum_{i=0}^{\infty}\lambda^i = \frac{1}{1-\lambda}
```
So we can scales the sum to normalise is. It is called $`\lambda`$-return
```math
G_{\lambda}(t) = (1-\lambda)\sum_{n=1}^\infty\lambda^{n-1}G^{(n)}(t)
```
Assume the episode ends at time T.  
When we reach T - t any N-step return is the full MC return, G(t)
```math
G_{\lambda}(t) = (1-\lambda)\sum_{n=1}^{T-t-1}\lambda^{n-1}G^{(n)}(t) + (1 - \lambda)\sum_{n = T - t}^{\infty}\lambda^{n-1}G(t)
```
It is because $`G^{(n)}`$ looks n steps ahead so we can not do that when we are more than half way through the episode.  
We can now simplify it to get
```math
G_{\lambda}(t) = (1-\lambda)\sum_{n=1}^{T-t-1}\lambda^{n-1}G^{(n)}(t) + (1 - \lambda)G(t)\lambda^{-1}\sum_{n = T - t}^{\infty}\lambda^{n}
```
We can simplify further:
```math
\sum_{n=0}^\infty \lambda^n = \frac{1}{1 - \lambda}, \;\sum_{n=0}^N \lambda^n = \frac{1 - \lambda^{N+1}}{1-\lambda} \longrightarrow \sum_{n = N + 1}^\infty\lambda^n = \frac{\lambda^{N + 1}}{1 - \lambda}
```
We use these to get
```math
G_{\lambda}(t) = (1-\lambda)\sum_{n=1}^{T-t-1}\lambda^{n-1}G^{(n)}(t) + (1 - \lambda)G(t)\lambda^{-1}\frac{\lambda^{T-t}}{1-\lambda}
```
and further
```math
G_{\lambda}(t) = (1-\lambda)\sum_{n=1}^{T-t-1}\lambda^{n-1}G^{(n)}(t) + \lambda^{T-t-1}G(t)
```
$`\lambda = 0`$ gives TD(0), $`\lambda = 1`$ gives Monte Carlo.  
For TD(0):  
target: $`R_{t+1} + \gamma V(S_{t+1})`$  
prediction: $`V(S_t)`$  
differnce: $`\delta_t = R_{t+1} + \gamma V(S_{t+1}) - V(S_t)`$  
parameter update: $`\theta_{t+1} = \theta_t + \alpha\delta_t\Delta_\theta V(S_t)`$  
Eligibility trace is a vector of the same size as a parameter vecotr. $`\theta_t \in \R^D`$ and $`e_t \in \R^D`$.
Eligibility vector works like momentum and keeps track of old gradients.  
$`\lambda`$ tells us how much of the past we want to keep.
```math
e_0 = 0,\; e_t = \nabla_{\theta} V(S_t) + \gamma\lambda e_{t-1}
```
Update now only depends on the next state. We don't have to wait for N steps to do the update. N-step method and true lambda return require waiting for future rewards and are called "forward view". In TD($`\lambda`$) we update the current param based on past errors and we call it "backward view".














